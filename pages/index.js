import React, { Component } from "react";
import fetch from "isomorphic-unfetch";
import Navbar from "../components/nav";
import Footer from "../components/footer";
import SearchBar, { TopSearches } from "../components/searchbar";
import FeaturesDMI from "../components/features";
import AboutCompass from "../components/about-compass";
import HiringDesc from "../components/hiring-desc";
import MethodDesc from "../components/methodology-desc";
import Testimonials from "../components/testimonials";
import HomeCounter from "../components/home-counter";
import Head from "next/head";
import "../scss/searchbar.scss";

class IndexPage extends Component {
  state = {};

  static async getInitialProps() {
    const url = await fetch(
      `https://product-page-api.herokuapp.com/api/category/allData`
    );
    const data = await url.json();

    const newdata = {
      data: data
    }

    return newdata
  }

  render() {
    return (
      <div>
        <Head>
          <title>DataM Intelligence</title>
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <meta
            name="description"
            content="DataM Intelligence 4Market Research is a market intelligence platform which gives access to syndicated, customised reports to its clients at one place."
          />
          <meta property="og:locale" content="en_US" />
          <meta property="og:type" content="website" />
          <meta property="og:title" content="DataM Intelligence" />
          <meta property="og:description" content="DataM Intelligence 4Market Research is a market intelligence platform which gives access to syndicated, customised reports to its clients at one place." />
          <meta property="og:url" content="https://www.datamintelligence.com/" />
          <meta property="og:site_name" content="DataMIntelligence" />
          <meta name="twitter:card" content="summary_large_image" />
          <meta name="twitter:description" content="DataM Intelligence 4Market Research is a market intelligence platform which gives access to syndicated, customised reports to its clients at one place." />
          <meta name="twitter:title" content="DataM Intelligence" />
          <meta name="twitter:site" content="@datam_research" />
          <meta name="twitter:image" content="https://datamintelligence.com/wp-content/uploads/2018/07/Agriculture1.png" />
          <meta name="twitter:creator" content="@datam_research" />
        </Head>
        <div className="home-main-box" style={{ overflowX: "hidden" }}>
          <div>
            <div className="top-box">
              <Navbar searchProps={this.props.data} />
              <div className="home-top-section ">
                <SearchBar searchProps={this.props.data} >
                  <TopSearches />
                </SearchBar>
              </div>
            </div>
            <FeaturesDMI />
            <AboutCompass />
            <HiringDesc />
            <MethodDesc />
            <Testimonials />
            <HomeCounter />
          </div>
          <Footer />
        </div>
      </div>
    );
  }
}

export default IndexPage;

{/* "dev": "node server.js",
    "build": "next build",
    "start": "next start -p $PORT node server.js",
    
 "scripts": {   "dev": "node server.js",
 "build": "./node_modules/next/dist/bin/next build",
 "heroku-postbuild": "./node_modules/next/dist/bin/next build",
  "start": "NODE_ENV=production node server.js"
 }, */}
