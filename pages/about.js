import React, { Component } from "react";
import Head from "next/head";
import Navbar from "../components/nav";
import Footer from "../components/footer";
import "../scss/about-dmi.scss";

const data = [
  {
    statement:
      "A US-based Medical Device manufacturing company whose flagship product placement in the Asian Market was enhanced by beneficial Product Marketing Strategies,Product Launches,Technology Trends and Competitive Benchmarking from DMI."
  },
  {
    statement:
      "A GCC based Feed company wanting to enter a new market in Asia-Pacific whose impactful entrance was  fuelled by DMI’s analysis of Industry Competitors,Market Entry Potential,Market Dynamics and Investment Analysis."
  },
  {
    statement:
      "A Western European Airline Caterer whose successful expansion into Central Europe and Asia was boosted by our Supply Chain Analysis,Market Segmentation by application,type and region,Porters Analysis,Analysis of Vendor Market Shares,Key Vendors and Future Outlook."
  }
];

class AboutPage extends React.Component {
  state = {
    value: 0
  };

  static async getInitialProps() {
    const url = await fetch(
      `https://product-page-api.herokuapp.com/api/category/allData`
    );
    const data = await url.json();

    const newdata = {
      data: data
    }

    return newdata
  }

  handleBack = () => {
    if (this.state.value !== 1 && this.state.value > 0) {
      this.setState({
        value: this.state.value - 1
      });
    } else if (this.state.value === 0) {
      this.setState({
        value: data.length - 1
      });
    } else {
      this.setState({
        value: 0
      });
    }
  };
  handleNext = () => {
    if (
      this.state.value !== data.length - 1 &&
      this.state.value < data.length - 1
    ) {
      this.setState({
        value: this.state.value + 1
      });
    } else if (this.state.value === data.length - 1) {
      this.setState({
        value: 0
      });
    } else {
      this.setState({
        value: data.length - 1
      });
    }
  };
  render() {
    return (
      <div>
        <Head>
          <title>About DataM Intelligence</title>
        </Head>
        <div className="about-dmi-main-box">
          <div className="with-background-box">
            <Navbar searchProps={this.props.data} />
            <div className="dmi-quote-box">
              <img src="/static/icons/quote.png" className="quote-img" />
              <img
                src="/static/Images/dmi-logo 2.png"
                className="dmi-log-about-page"
              />
              <h3>
                We at DMI make sure your business is consistently remunerative
                with our extensive knowledge and profound understanding of the
                market.
              </h3>
              <button>GET IN TOUCH WITH US</button>
            </div>
          </div>
          <div className="dmi-misson-box">
            <p>
              <span>MISSION-ONE</span>“To empower clients with advanced Research &amp; Consulting services
that will enhance their competitiveness across different geographies in
their respective industry domains which in turn enable them to meet
their business goals “
            </p>
            <p style={{ marginTop: "5rem" }}>
              <span>CORE VALUES</span>
              <ul style={{ display: "inline-block", listStyleType: "none", paddingLeft: "2.4rem" }}>
                <li>- Being Remarkable, Taking Ownership, Nurturing & Trust and at the end Execution Matters </li>
                <li> - We are inclusive and collaborative & executional excellence is the core value of our firm</li>
                <li>  - We are obsessed of being responsible and delivering premium value to our customers</li>
              </ul>
            </p>
          </div>
          <div className="we-are-dmi-box">
            <div className="we-are-dmi-top">
              <div className="we-are-dmi-left">
                <img src="/static/Images/WE ARE DMI 2.png" />
                <p>
                  DataM Intelligence was incorporated in the early weeks of 2017 as a Market Research and
  Consulting firm with just two people on board. Within a span of less than a year we have secured
more than 100 unique customers from established organizations all over the world.</p><p>
                  DataM Intelligence has set its standards in the market which resulted in having our own set of
                  recurring clients who are willing to invest in us and build business relationships with customers
                  across the globe. We started working with clients on customized reports and providing market
intelligence insights pulling ahead of our competitors.</p><p>
                  The Company has taken up many consult projects during this growth period, which in turn paved the
                  way for introducing new launches in forthcoming years.
                </p>
              </div>
              <div className="we-are-dmi-right">
                <img src="/static/Images/we-are-dmi.jpg" />
              </div>
            </div>
            <div className="we-are-dmi-bottom">
              <span>2017</span>
              <p>
                DataM Intelligence was established on an elemental idea of publishing Advanced Market Research
  Reports with precise data points &amp; utmost accuracy that will accelerate the decision making in
designing disruptive solutions<br />
                <br />
                We monitor and analyse the market by dissecting various parameters such as market influencers,
                competitive intensity, innovations, trends, and emerging products. This aids us in providing the
detail scenario of the present and future of the market.<br /><br />
                We have evolved by building our own strong syndicated business arm through which we publish
                market reports that have been extensively researched by our in-house team of analysts. These
                unique solutions cater to every business segment, irrespective of the size of the organization
              </p>
            </div>
          </div>
          <div className="we-thrive-to-serve-box">
            <div className="padding-serve" />
            <div className="serve-main">
              <h2>WE THRIVE TO SERVE</h2>
              <p>
                We serve various individuals and entities across B2B which include but not limited to Industry
  Investors &amp; Investment Bankers, Research Professionals, Emerging Companies, Raw Material
  Suppliers/ Buyers, Product Suppliers/ Buyers, Research Institutes </p><p>
  Our services help clients to make informed strategic decisions to leverage expertise and minimalize
  risks by
  
              </p>
              <ul style={{fontSize: "1.4rem", listStyleType: "none", lineHeight: "1.4"}}>
                <li>- Ascertaining target potential customers</li>
                <li>- Identifying key market trends and competitor strategies</li>
                <li>- Analysing competitors’ products, pricing, financials, sales and strategy</li>
                <li>- Identifying investment opportunities</li>
                <li>- Interpreting Market Entry Prospects</li>
              </ul>
              <br/>
             <ul style={{fontSize: "1.4rem", listStyleType: "none", lineHeight: "1.4"}}>
               <li><span style={{fontFamily: "bebas neue", fontSize:"2.2rem", color: "#ff6600", paddingRight: "0.5rem"}}>Syndicated Research</span>
                  DMI’s syndicated research reports provide leading-edge research solutions. We maintain a
                  repository of comprehensive reports across varied industrial domains
                </li>
               <li><span style={{fontFamily: "bebas neue", fontSize:"2.2rem", color: "#ff6600", paddingRight: "0.5rem"}}>Custom Research</span>
                  DMI’s custom research services typically deals with creation of tailored and client-centric
                  reports. Research involves extensive secondary and primary research combined with expert
                  validations to provide clients highly reliable specified data. It typically encompasses Granular
                  Market Segmentation, Market Opportunity Analysis, Competitive Analysis, Product
                  Assessment, Market Access Strategies, Product Life cycle Management Strategy, Consumer
                  Surveys
               </li>
               <li><span style={{fontFamily: "bebas neue", fontSize:"2.2rem", color: "#ff6600", paddingRight: "0.5rem"}}>Market Intelligence Insights</span>
DMI’s Market Intelligence holds high expertise in analyzing market landscape and
monitoring competitive landscape. Pipeline Portfolio Tracking, CTAnalysis, Competitor
Product Monitoring, Strategic Collaborations Landscape, Competitor Analysis</li>
             </ul>
            </div>
          </div>
          <div className="our-forte-box">
            <span>OUR FORTE</span>
            <div className="our-forte-container">
              <button className="slide-back-btn" onClick={this.handleBack}>
                <img src="/static/Images/back.svg" />
              </button>
              <div className="slider-box">
                <div
                  className="our-forte-slider"
                  style={{
                    transform: `translateX(-${this.state.value * 100}%)`,
                    transition: "transform 0.5s ease-in-out"
                  }}
                >
                  {data.map((data, index) => (
                    <p
                      key={index}
                      className={`${this.state.value === index &&
                        "active-statement"}`}
                    >
                      {data.statement}
                    </p>
                  ))}
                </div>
              </div>
              <button className="slide-next-btn" onClick={this.handleNext}>
                <img src="/static/Images/next.svg" />
              </button>
            </div>
          </div>
          <div>
            <img src="/static/Images/clients.jpg" width="100%"/>
          </div>
          <Footer />
        </div>
      </div >
    );
  }
}

export default AboutPage;
