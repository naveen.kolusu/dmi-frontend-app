webpackHotUpdate("static/development/pages/product.js",{

/***/ "./pages/product.js":
/*!**************************!*\
  !*** ./pages/product.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "./node_modules/next/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! isomorphic-unfetch */ "./node_modules/isomorphic-unfetch/browser.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _components_buy_report__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/buy-report */ "./components/buy-report.jsx");
/* harmony import */ var _components_product_main__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/product-main */ "./components/product-main.jsx");
/* harmony import */ var _components_related_reports_sec__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/related-reports-sec */ "./components/related-reports-sec.jsx");
/* harmony import */ var _components_report_data_sec__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/report-data-sec */ "./components/report-data-sec.jsx");
/* harmony import */ var _components_bundle_report__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/bundle-report */ "./components/bundle-report.jsx");
/* harmony import */ var _components_layout__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../components/layout */ "./components/layout.jsx");
/* harmony import */ var _components_breadcrumb__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../components/breadcrumb */ "./components/breadcrumb.jsx");
/* harmony import */ var _scss_product_page_scss__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../scss/product-page.scss */ "./scss/product-page.scss");
/* harmony import */ var _scss_product_page_scss__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_scss_product_page_scss__WEBPACK_IMPORTED_MODULE_13__);


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }















var ProductPageTemplate =
/*#__PURE__*/
function (_Component) {
  _inherits(ProductPageTemplate, _Component);

  function ProductPageTemplate() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, ProductPageTemplate);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(ProductPageTemplate)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      keyURL: _this.props.data.slug,
      request: {
        name: "",
        email: "",
        phone: "",
        message: "",
        product_title: "",
        url: ""
      },
      bgImage: "product-page-bg.jpg",
      PDate: ""
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleChangeRequest", function (event) {
      var request = _objectSpread({}, _this.state.request);

      request[event.target.name] = event.target.value; // request.product_title = document.title;
      // request.url = window.location.href;

      _this.setState({
        request: request
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleRequestSubmit",
    /*#__PURE__*/
    _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var request;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              request = _objectSpread({}, _this.state.request);
              request.product_title = " " + document.title;
              request.url = " " + window.location.href;

              _this.setState({
                request: request
              }, function () {
                var formAPI = "https://product-page-api.herokuapp.com/api/products/product/smtp";
                var newData = _this.state.request;

                var _axios$post = axios__WEBPACK_IMPORTED_MODULE_5___default.a.post(formAPI, newData),
                    reply = _axios$post.data;

                console.log(newData);
                console.log(reply);
              });

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    })));

    return _this;
  }

  _createClass(ProductPageTemplate, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var date = this.props.data.created;
      var fullDate = this.props.data.created.substring(0, date.indexOf("T")); // const data = this.giveDate(fullDate);

      this.setState({
        PDate: fullDate
      });
    } // giveDate = date => {
    //   const data = date.indexOf("T");
    //   return data.substring(data);
    // };

  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("title", null, this.props.data.Meta_title), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
        name: "description",
        content: this.props.data.Meta_Desc
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
        name: "keywords",
        content: this.props.data.keywordArea.keyword
      })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        style: {
          backgroundImage: "url(/static/Images/product-page-bg.jpg)",
          backgroundRepeat: "no-repeat"
        }
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_layout__WEBPACK_IMPORTED_MODULE_11__["default"], {
        bgImage: this.state.bgImage
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_breadcrumb__WEBPACK_IMPORTED_MODULE_12__["default"], null), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "clearfix"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_product_main__WEBPACK_IMPORTED_MODULE_7__["default"], {
        Title: this.props.data.Product_Title,
        ImagePath: this.props.data.ImagePath,
        PublishedDate: this.state.PDate
      })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "clearfix container"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "left-box"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_report_data_sec__WEBPACK_IMPORTED_MODULE_9__["default"], {
        TabsData: this.props.data.Tabs
      })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "right-box"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_buy_report__WEBPACK_IMPORTED_MODULE_6__["default"], {
        Prices: this.props.data.Price
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_bundle_report__WEBPACK_IMPORTED_MODULE_10__["default"], null), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_related_reports_sec__WEBPACK_IMPORTED_MODULE_8__["default"], null)))))));
    }
  }], [{
    key: "getInitialProps",
    value: function () {
      var _getInitialProps = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(_ref2) {
        var req, res, keyurl, url, data;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                req = _ref2.req, res = _ref2.res, keyurl = _ref2.query.keyurl;
                _context2.next = 3;
                return isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_3___default()("https://product-page-api.herokuapp.com/api/products/product/".concat(keyurl));

              case 3:
                url = _context2.sent;
                _context2.next = 6;
                return url.json();

              case 6:
                data = _context2.sent;

                if (!(data === null)) {
                  _context2.next = 14;
                  break;
                }

                res.statusCode = 404;
                res.set("application/html").send("<h1>Page not found</h1>\n      ");
                res.end("".concat(react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", null, "Page not found")));
                return _context2.abrupt("return");

              case 14:
                return _context2.abrupt("return", {
                  data: data
                });

              case 15:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function getInitialProps(_x) {
        return _getInitialProps.apply(this, arguments);
      }

      return getInitialProps;
    }()
  }]);

  return ProductPageTemplate;
}(react__WEBPACK_IMPORTED_MODULE_1__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(next_router__WEBPACK_IMPORTED_MODULE_4__["withRouter"])(ProductPageTemplate)); // ProductPageTemplate.getInitialProps = async function(context) {
//   const { keyurl } = context.router.query.keyurl;
//   const urlkey = `https://product-page-api.herokuapp.com/api/products/product/${keyurl}`;
//   console.log({ keyurl });
//   const res = await fetch({ urlkey });
//   const data = await res.json();
//   console.log(`Show data fetched. Count: ${data}`);
//   return {
//     data
//   };
// };
// import { withRouter } from "next/router";
// const Page = withRouter(props => (
//   <div>
//     <h1>{props.router.query.title}</h1>
//     <p>This is the blog post content.</p>
//   </div>
// ));
// export default Page;
    (function (Component, route) {
      if(!Component) return
      if (false) {}
      module.hot.accept()
      Component.__route = route

      if (module.hot.status() === 'idle') return

      var components = next.router.components
      for (var r in components) {
        if (!components.hasOwnProperty(r)) continue

        if (components[r].Component.__route === route) {
          next.router.update(r, Component)
        }
      }
    })(typeof __webpack_exports__ !== 'undefined' ? __webpack_exports__.default : (module.exports.default || module.exports), "/product")
  
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=product.js.299dac9305c7caafb722.hot-update.js.map