import React from "react";
import Fuse from "fuse.js";
import Link from "next/link"
import SearchPage from "../components/search"

class SearchBar extends React.Component {
  state = {

    searchInput: "",
    searchOutput: [],
  }

  handleChange = (e) => {
    const data = this.props.searchProps;
    console.log("newData", data);
    const options = {
      keys: ['Product_Title']
    }
    let fuse = new Fuse(data, options);
    let fuseData = fuse.search(e.target.value);
    console.log(fuseData);
    this.setState({
      searchInput: e.target.value,
      searchOutput: fuseData
    })
  }
  render() {
    return (
      <div style={{ width: "100%" }}>
        <div className="search-box">
          <span className="border-left" />
          <SearchPage handleChange={this.handleChange} searchInput={this.state.searchInput} searchOutput={this.state.searchOutput} />
          <button>Search</button>
          {this.props.children}
        </div>
        <div className="icon-box">
          <div className="icon-container">
            <div><Link as={`/category/pipeline-analysis/agriculture`} href={`/mainCategory?main=pipeline-analysis&&product?=agriculture`}>
              <a><img src="/static/icons/agriculture.svg" /><span>Agriculture</span></a></Link></div>
            <div><Link as={`/category/pipeline-analysis/animal-health`} href={`/mainCategory?main=pipeline-analysis&&product?=animal-health`}>
              <a><img src="/static/icons/animal health.svg" /><span>Animal Health</span></a></Link></div>
            <div><Link as={`/category/pipeline-analysis/automotive`} href={`/mainCategory?main=pipeline-analysis&&product?=automotive`}>
              <a><img src="/static/icons/automotive.svg" /><span>Automotive</span></a></Link></div>
            <div><Link as={`/category/pipeline-analysis/energy-utilities`} href={`/mainCategory?main=pipeline-analysis&&product?=energy-utilities`}>
              <a><img src="/static/icons/energy and utilities.svg" /><span>Energy & Utilities</span></a></Link></div>
            <div><Link as={`/category/pipeline-analysis/food-beverages`} href={`/mainCategory?main=pipeline-analysis&&product?=food-beverages`}>
              <a><img src="/static/icons/food and beverages.svg" /><span>Food & Beverages</span></a></Link></div>
            <div><Link as={`/category/pipeline-analysis/healthcare-services`} href={`/mainCategory?main=pipeline-analysis&&product?=healthcare-services`}>
              <a><img src="/static/icons/healthcare.svg" /><span>Healthcare Services</span></a></Link></div>
            <div><Link as={`/category/pipeline-analysis/information-security`} href={`/mainCategory?main=pipeline-analysis&&product?=information-security`}>
              <a><img src="/static/icons/information-security.svg" /><span>Information Security</span></a></Link></div>
            <div><Link as={`/category/pipeline-analysis/medical-devices`} href={`/mainCategory?main=pipeline-analysis&&product?=medical-devices`}>
              <a><img src="/static/icons/medical devices.svg" /><span>Medical Devices</span></a></Link></div>
            <div><Link as={`/category/pipeline-analysis/metals-mining`} href={`/mainCategory?main=pipeline-analysis&&product?=metals-mining`}>
              <a><img src="/static/icons/flask-boiling.svg" /><span>Metals & Mining</span></a></Link></div>
            <div><Link as={`/category/pipeline-analysis/petrochemicals`} href={`/mainCategory?main=pipeline-analysis&&product?=petrochemicals`}>
              <a><img src="/static/icons/petrochemicals.svg" /><span>Petrochemicals</span></a></Link></div>
            <div><Link as={`/category/pipeline-analysis/pharmaceuticals`} href={`/mainCategory?main=pipeline-analysis&&product?=pharmaceuticals`}>
              <a><img src="/static/icons/pills.svg" /><span>Pharmaceuticals</span></a></Link></div>
          </div>
        </div >
      </div >
    );
  }
}

const topwords = [
  { name: "Prostate Cancer", slug: "prostate-cancer-market" },
  { name: "Optical Coherence Tomography", slug: "optical-coherence-tomography-market" },
  { name: "Oats", slug: "oats-market" },
  { name: "Defoamers Market", slug: "defoamers_market" },
  { name: "Type 2 Diabetes", slug: "/type-2-diabetes" },
  { name: "Anti Aging ", slug: "anti-aging-market/" },
  { name: "Metered Dose Inhaler Devices ", slug: "metered-dose-inhaler-devices-market" },
  { name: "Stevia", slug: "stevia-market" },
  { name: "Infusion Systems ", slug: "infusion-systems-market" },
  { name: "Needle Free Injection ", slug: "needle-free-injection-market" },
  { name: "Ehealth", slug: "ehealth-market" },
  { name: "Blood Pressure Monitors ", slug: "blood-pressure-monitors-market" },
  { name: "Clinical It System", slug: "Clinical It Systems" }
];

export const TopSearches = () => {
  return (
    <div className="top-searches">
      <h3>Top Searches of the week</h3>
      <div className="top-words clearfix">
        {topwords.map((topword, index) => (
          <Link as={`/research-report/${topword.slug}`} href={`/product?keyurl=${topword.slug}`} key={index}><a><p >{topword.name}</p></a></Link>
        ))}
      </div>
    </div>
  );
};

export default SearchBar;
