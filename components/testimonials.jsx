import React from "react";
import "../scss/testimonials.scss";
const data = [
  {
    name: "DR. ALBERTO ARNAVAS",
    designetion: "Director General",
    company: "EUROPEAN FEDERATION FOR CONSTRUCTION CHEMICALS",
    message:
      " EFCC had an interesting business experience with DataM Intelligence : despite the complex matter, DataM Intelligence delivered the requested data & info quite promptly, showing professionalism and flexibility in the effort to match our requirements "
  },
  {
    name: "RETO MARX",
    designetion: "CEO & Founder",
    company: " Warisan Investment Partners",
    message:
      " Great team, working closely with and listening carefully to the customers requests to tailor reports exactly to their needs. The short timeframe to complete a report was outstanding "
  },
  {
    name: "Guillermo Pons Zolessi",
    designetion: "Gerente comercial",
    company: "EFICE",
    message:
      " Really our experience with DataM Intelligence was excellent. From the first moment the attention was superlative, very simple how to make the payment, and the most important, the report, was just what we were needing to complete a market study. Highly recommended services."
  },
  {
    name: "Pham Tuan Minh",
    designetion: "Research Development Director",
    company: "Axis Research Ltd.",
    message:
      " We work regularly with DataM Intelligence for purchasing various industry reports. The analysts at DataM are professional. They follow strictly required research objectives and have never missed our deadline. They also offer great support after purchase. DataM Intelligence is good research partner for every business and organization."
  }
];

class Testimonials extends React.Component {
  state = {
    value: 0
  };
  handleBack = () => {
    if (this.state.value !== 1 && this.state.value > 0) {
      this.setState({
        value: this.state.value - 1
      });
    } else if (this.state.value === 0) {
      this.setState({
        value: data.length - 1
      });
    } else {
      this.setState({
        value: 0
      });
    }
  };
  handleNext = () => {
    if (
      this.state.value !== data.length - 1 &&
      this.state.value < data.length - 1
    ) {
      this.setState({
        value: this.state.value + 1
      });
    } else if (this.state.value === data.length - 1) {
      this.setState({
        value: 0
      });
    } else {
      this.setState({
        value: data.length - 1
      });
    }
  };
  render() {
    return (
      <div className=" testimonials-box ">
        <div className="words-of-trust">
          <h4>Words of Trust</h4>
          <p>
            It is a long established fact that a reader will be distracted by
            the readable content of a page when looking at its layout. The point
            of using Lorem Ipsum is that it has a more-or-less normal
            distribution of letters, as opposed to using 'Content here, content
            here', making it look like readable English. It is a long
            established fact that a reader will be distracted by the readable
            content of a page when looking at its layout. The point of using
            Lorem Ipsum is that it has a more-or-less normal distribution of
            letters, as opposed to using 'Content here, content here', making it
            look like readable English. It is a long established fact that a
            reader will be distracted by the readable content of a page when
            looking at its layout. The point of using Lorem Ipsum is that it has
            a more-or-less normal distribution of letters, as opposed to using
            'Content here, content here', making it look like readable English.
          </p>
        </div>
        <div className="testimonials-slider">
          <div className="slide-content">
            <button onClick={this.handleBack} className="slide-back-btn">
              <img src="/static/Images/back.svg" />
            </button>
            <div className="testimonials-slides">
              <div
                className="testimonials-content"
                style={{
                  transform: `translateX(-${this.state.value * 100}%)`,
                  transition: "transform 0.5s ease-in-out"
                }}
              >
                {data.map((info, index) => (
                  <div key={index}
                    className={`testimonial-item ${this.state.value === index &&
                      "active-testimonial"}`}
                  // style={{ minWidth: `${100 / data.length}%` }}
                  >
                    <h4>{info.name}</h4>
                    <p>{info.designetion}</p>
                    <p>
                      <b>{info.company}</b>
                    </p>
                    <p>{info.message}</p>
                  </div>
                ))}
              </div>
            </div>

            <button onClick={this.handleNext} className="slide-next-btn">
              <img src="/static/Images/next.svg" />
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Testimonials;
