import React from "react";
import Link from "next/link"
import "../scss/hiring-desc.scss";
const HiringDesc = () => {
  return (
    <div className="hiring-section">
      <div className="hiring-container">
        <div className="hiring-content ">
          <h3>GOING WIRELESS WITH YOUR HEADPHONES</h3>
          <p>
            DataM Intelligence 4Market Research is a market intelligence platform which gives access to syndicated, customised reports to its clients at one place. As a firm with rich experience in research and consulting across multiple domains we are one stop solution that will cater to the needs of clients in key business areas.</p>
          <p>DMI benefits thousands of companies by helping them take their innovations early to the market by providing a complete view of the market with statistical forecasts.</p>
          <p> We Help you in building up your Career.</p>
          <p>Join Us.
          </p>
          <Link href="/join-us"><a><button>APPLY NOW</button></a></Link>
        </div>
      </div>
      <div className="hiring-title">
        <h2>WE ARE HIRING</h2>
      </div>
    </div >
  );
};

export default HiringDesc;
