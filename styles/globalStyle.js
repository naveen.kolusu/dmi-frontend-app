import css from "styled-jsx/css";
import normalize from "normalize.css/normalize.css";

export default css`
  ${normalize}
`;
