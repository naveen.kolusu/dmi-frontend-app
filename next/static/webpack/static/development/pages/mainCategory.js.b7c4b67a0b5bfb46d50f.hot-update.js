webpackHotUpdate("static/development/pages/mainCategory.js",{

/***/ "./components/nav.jsx":
/*!****************************!*\
  !*** ./components/nav.jsx ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_search__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/search */ "./components/search.jsx");
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! fuse.js */ "./node_modules/fuse.js/dist/fuse.js");
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(fuse_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! isomorphic-unfetch */ "./node_modules/isomorphic-unfetch/browser.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _scss_navbar_scss__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../scss/navbar.scss */ "./scss/navbar.scss");
/* harmony import */ var _scss_navbar_scss__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_scss_navbar_scss__WEBPACK_IMPORTED_MODULE_5__);


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






var ivData = [{
  name: "agriculture",
  ulData: [{
    name: "agrochemicals"
  }, {
    name: "seed"
  }, {
    name: "testing-technology"
  }]
}, {
  name: "animal-health",
  ulData: [{
    name: "feeds"
  }, {
    name: "veterinary"
  }]
}, {
  name: "automotive"
}, {
  name: "aviation"
}, {
  name: "energy-utilities",
  ulData: [{
    name: "power-generation"
  }]
}, {
  name: "food-beverages",
  ulData: [{
    name: "additives"
  }, {
    name: "beverages"
  }, {
    name: "processed-food"
  }, {
    name: "supplement"
  }]
}, {
  name: "healthcare-services",
  ulData: [{
    name: "healthcare-it"
  }]
}, {
  name: "information-communication"
}, {
  name: "medical-devices",
  ulData: [{
    name: "cardiovascular-devices"
  }, {
    name: "surgical-devices"
  }, {
    name: "wound-care"
  }]
}, {
  name: "metals-mining"
}, {
  name: "petrochemicals",
  ulData: [{
    name: "adhesives-sealants"
  }, {
    name: "advanced-materials"
  }, {
    name: "metals-ceramics"
  }, {
    name: "paints-coatings"
  }, {
    name: "seacial-fine-chemicals"
  }, {
    name: "sspeciality-chemicals"
  }, {
    name: "polymers"
  }, {
    name: "water-treatment"
  }]
}, {
  name: "pharmaceuticals",
  ulData: [{
    name: "oncology"
  }, {
    name: "ophthalmology"
  }]
}];
var paData = [{
  name: "pharmaceuticals-pa",
  ulData: [{
    name: "indication"
  }, {
    name: "moa"
  }, {
    name: "molecular-type"
  }]
}];

var NavBar =
/*#__PURE__*/
function (_React$Component) {
  _inherits(NavBar, _React$Component);

  function NavBar() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, NavBar);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(NavBar)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      // navBlock: true,
      searchBlock: false,
      searchInput: "",
      searchOutput: [],
      itemsToShow: {
        cats: {
          toggle: false,
          value: 0
        },
        subCats: {
          toggle: false,
          value: -1
        }
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleChange", function (e) {
      var data = _this.props.searchProps;
      console.log("newData", data);
      var options = {
        keys: ['Product_Title']
      };
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3___default.a(data, options); // const fuseData = [
      //   {
      //     Product_Title: "hello",
      //     slug: "hello"
      //   }
      // ]

      var fuseData = fuse.search(e.target.value);
      console.log(fuseData);

      _this.setState({
        searchInput: e.target.value,
        searchOutput: fuseData
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleClick", function (e) {
      if (e.target.name === "searchIcon") {
        _this.setState({
          // navBlock: false,
          searchBlock: true
        });
      } else {
        _this.setState({
          // navBlock: true,
          searchBlock: false
        });
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleToggle", function (cats, value) {
      var itemsToShow = _objectSpread({}, _this.state.itemsToShow);

      itemsToShow[cats].toggle = !itemsToShow[cats].toggle;
      itemsToShow[cats].value = value;

      _this.setState({
        itemsToShow: itemsToShow
      });

      console.log(itemsToShow);
    });

    return _this;
  }

  _createClass(NavBar, [{
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(newProps) {
      this.setState({
        searchBlock: false,
        searchInput: ""
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("nav", {
        className: "clearfix"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "image-box"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/static/Images/DMI-Logo-White.png",
        alt: "DMI-Logo"
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "nav-items-box"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
        className: "clearfix main-ul"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Home")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        as: "/category/industry-verticals",
        href: "/mainCategory?main=industry-verticals"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Industry Verticals"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: function onClick() {
          return _this2.handleToggle("cats", 1);
        },
        className: "arrow-down"
      }, "^"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
        className: " ".concat(this.state.itemsToShow.cats.value === 1 && "nav-cats")
      }, ivData.map(function (iv, ivindex) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
          key: ivindex,
          className: "nav-cats-item"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
          as: "/category/industry-verticals/".concat(iv.name),
          href: "/mainCategory?main=industry-verticals&&product?=".concat(iv.name)
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, iv.name))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
          onClick: function onClick() {
            return _this2.handleToggle("subCats", ivindex);
          },
          className: iv.ulData && "arrow"
        }, "^"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
          className: " ".concat(_this2.state.itemsToShow.subCats.value === ivindex && "nav-cats-inner")
        }, iv.ulData && iv.ulData.map(function (ul, ulindex) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
            key: ulindex,
            className: "nav-cats-item-inner"
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
            as: "/category/industry-verticals/".concat(iv.name, "/").concat(ul.name),
            href: "/mainCategory?main=industry-verticals&&product?=".concat(iv.name, "&&sub").concat(iv.name, "/").concat(ul.name)
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, ul.name))));
        })));
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        as: "/category/pipeline-analysis",
        href: "/mainCategory?main=pipeline-analysis"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Pipeline Analysis"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: function onClick() {
          return _this2.handleToggle("cats", 2);
        },
        className: "arrow-down"
      }, "^"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
        className: " ".concat(this.state.itemsToShow.cats.value === 2 && this.state.itemsToShow.cats.toggle === true && "nav-cats")
      }, paData.map(function (pa, paindex) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
          key: paindex,
          className: "nav-cats-item"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
          as: "/category/pipeline-analysis/".concat(pa.name),
          href: "/mainCategory?main=pipeline-analysis&&product?=".concat(pa.name)
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, pa.name))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
          onClick: function onClick() {
            return _this2.handleToggle("subCats", paindex);
          },
          className: pa.ulData && "arrow"
        }, "^"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
          className: " ".concat(_this2.state.itemsToShow.subCats.value === paindex && "nav-cats-inner")
        }, pa.ulData && pa.ulData.map(function (ul, ulindex) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
            key: ulindex,
            className: "nav-cats-item-inner"
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
            as: "/category/pipeline-analysis/".concat(pa.name, "/").concat(ul.name),
            href: "/mainCategory?main=pipeline-analysis&&product?=".concat(pa.name, "&&sub").concat(pa.name, "/").concat(ul.name)
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, ul.name))));
        })));
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Our Methodology")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        href: "/contact"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Contact Us")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        href: "/about"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "About Us")))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "nav-icons-box clearfix"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/static/icons/shopping-cart.svg",
        alt: "shopping-cart-icon"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/static/icons/search.svg",
        alt: "search-icon",
        onClick: this.handleClick,
        name: "searchIcon"
      }))), this.state.searchBlock === true && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "nav-searchbar"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_search__WEBPACK_IMPORTED_MODULE_2__["default"], {
        handleChange: this.handleChange,
        searchInput: this.state.searchInput,
        searchOutput: this.state.searchOutput
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: this.handleClick,
        className: "nav-search-close"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/static/icons/multiply.svg"
      }))));
    }
  }]);

  return NavBar;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

;
/* harmony default export */ __webpack_exports__["default"] = (NavBar);
{
  /* <ul>
                   <li>
                     <Link as={`/category/industry-verticals/agriculture`} href={`/mainCategory?main=industry-verticals&&product=agriculture`}>
                       <a>
                         <p>Agriculture</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals/agriculture/agrochemicals`} href={`/mainCategory?main=industry-verticals&&product=agriculture&&sub=agrochemicals`}>
                           <a>
                             <p>Agrochemicals</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals/agriculture/seed`} href={`/mainCategory?main=industry-verticals&&product=agriculture&&sub=seed`}>
                           <a>
                             <p>Seed</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals/agriculture/seed`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Testing and Technology</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Animal Health</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Feeds</p>
                           </a>
                         </Link>
                       </li>
                       <li><Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                         <a>
                           <p>Veterinary</p>
                         </a>
                       </Link>
                       </li>
                     </ul>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Automotive</p>
                       </a>
                     </Link>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Aviation</p>
                       </a>
                     </Link>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Energy and Utilities</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Power Generation</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li><Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                     <a>
                       <p>Food and Beverages</p>
                     </a>
                   </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Additives</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Beverages</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Processed Food</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Supplements</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Healthcare Services</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Healthcare IT</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Information & Communication</p>
                       </a>
                     </Link>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Medical Devices</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Cardiovascular Devices</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Surgical Devices</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Woundcare Management</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li><Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                     <a>
                       <p>Metals and Mining</p>
                     </a>
                   </Link>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Petrochemicals</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Adhesives & Sealants</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Advanced Materials</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Metals ceramics & Industrial Materials</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Paints and Coatings</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Special and Fine Chemicals</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Speciality Chemicals</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Polymers</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Water Treatment</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li><Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                     <a>
                       <p>Pharmaceuticals</p>
                     </a>
                   </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Oncology</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Ophthalmology</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                 </ul> */
}

/***/ })

})
//# sourceMappingURL=mainCategory.js.b7c4b67a0b5bfb46d50f.hot-update.js.map