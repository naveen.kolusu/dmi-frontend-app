module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 55);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("next/link");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/regenerator");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_search__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6);
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(7);
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(fuse_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(9);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _scss_navbar_scss__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(10);
/* harmony import */ var _scss_navbar_scss__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_scss_navbar_scss__WEBPACK_IMPORTED_MODULE_5__);


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






var ivData = [{
  name: "agriculture",
  ulData: [{
    name: "agrochemicals"
  }, {
    name: "seed"
  }, {
    name: "testing-technology"
  }]
}, {
  name: "animal-health",
  ulData: [{
    name: "feeds"
  }, {
    name: "veterinary"
  }]
}, {
  name: "automotive"
}, {
  name: "aviation"
}, {
  name: "energy-utilities",
  ulData: [{
    name: "power-generation"
  }]
}, {
  name: "food-beverages",
  ulData: [{
    name: "additives"
  }, {
    name: "beverages"
  }, {
    name: "processed-food"
  }, {
    name: "supplement"
  }]
}, {
  name: "healthcare-services",
  ulData: [{
    name: "healthcare-it"
  }]
}, {
  name: "information-communication"
}, {
  name: "medical-devices",
  ulData: [{
    name: "cardiovascular-devices"
  }, {
    name: "surgical-devices"
  }, {
    name: "wound-care"
  }]
}, {
  name: "metals-mining"
}, {
  name: "petrochemicals",
  ulData: [{
    name: "adhesives-sealants"
  }, {
    name: "advanced-materials"
  }, {
    name: "metals-ceramics"
  }, {
    name: "paints-coatings"
  }, {
    name: "seacial-fine-chemicals"
  }, {
    name: "sspeciality-chemicals"
  }, {
    name: "polymers"
  }, {
    name: "water-treatment"
  }]
}, {
  name: "pharmaceuticals",
  ulData: [{
    name: "oncology"
  }, {
    name: "ophthalmology"
  }]
}];
var paData = [{
  name: "pharmaceuticals-pa",
  ulData: [{
    name: "indication"
  }, {
    name: "moa"
  }, {
    name: "molecular-type"
  }]
}];

var NavBar =
/*#__PURE__*/
function (_React$Component) {
  _inherits(NavBar, _React$Component);

  function NavBar() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, NavBar);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(NavBar)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      // navBlock: true,
      searchBlock: false,
      searchInput: "",
      searchOutput: [],
      itemsToShow: {
        cats: {
          toggle: false,
          value: 0
        },
        subCats: {
          toggle: false,
          value: -1
        }
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleChange", function (e) {
      var data = _this.props.searchProps;
      console.log("newData", data);
      var options = {
        keys: ['Product_Title']
      };
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3___default.a(data, options); // const fuseData = [
      //   {
      //     Product_Title: "hello",
      //     slug: "hello"
      //   }
      // ]

      var fuseData = fuse.search(e.target.value);
      console.log(fuseData);

      _this.setState({
        searchInput: e.target.value,
        searchOutput: fuseData
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleClick", function (e) {
      if (e.target.name === "searchIcon") {
        _this.setState({
          // navBlock: false,
          searchBlock: true
        });
      } else {
        _this.setState({
          // navBlock: true,
          searchBlock: false
        });
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleToggle", function (cats, value) {
      var itemsToShow = _objectSpread({}, _this.state.itemsToShow);

      itemsToShow[cats].toggle = !itemsToShow[cats].toggle;
      itemsToShow[cats].value = value;

      _this.setState({
        itemsToShow: itemsToShow
      });

      console.log(itemsToShow);
    });

    return _this;
  }

  _createClass(NavBar, [{
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(newProps) {
      this.setState({
        searchBlock: false,
        searchInput: ""
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("nav", {
        className: "clearfix"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "image-box"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/static/Images/DMI-Logo-White.png",
        alt: "DMI-Logo"
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "nav-items-box"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
        className: "clearfix main-ul"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Home")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        as: "/category/industry-verticals",
        href: "/mainCategory?main=industry-verticals"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Industry Verticals"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: function onClick() {
          return _this2.handleToggle("cats", 1);
        },
        className: "arrow-down"
      }, "^"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
        className: " ".concat(this.state.itemsToShow.cats.value === 1 && this.state.itemsToShow.cats.toggle === true && "nav-cats")
      }, ivData.map(function (iv, ivindex) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
          key: ivindex,
          className: "nav-cats-item"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
          as: "/category/industry-verticals/".concat(iv.name),
          href: "/mainCategory?main=industry-verticals&&product?=".concat(iv.name)
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, iv.name))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
          onClick: function onClick() {
            return _this2.handleToggle("subCats", ivindex);
          },
          className: iv.ulData && "arrow"
        }, "^"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
          className: " ".concat(_this2.state.itemsToShow.subCats.value === ivindex && "nav-cats-inner")
        }, iv.ulData && iv.ulData.map(function (ul, ulindex) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
            key: ulindex,
            className: "nav-cats-item-inner"
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
            as: "/category/industry-verticals/".concat(iv.name, "/").concat(ul.name),
            href: "/mainCategory?main=industry-verticals&&product?=".concat(iv.name, "&&sub").concat(iv.name, "/").concat(ul.name)
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, ul.name))));
        })));
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        as: "/category/pipeline-analysis",
        href: "/mainCategory?main=pipeline-analysis"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Pipeline Analysis"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: function onClick() {
          return _this2.handleToggle("cats", 2);
        },
        className: "arrow-down"
      }, "^"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
        className: " ".concat(this.state.itemsToShow.cats.value === 2 && this.state.itemsToShow.cats.toggle === true && "nav-cats")
      }, paData.map(function (pa, paindex) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
          key: paindex,
          className: "nav-cats-item"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
          as: "/category/pipeline-analysis/".concat(pa.name),
          href: "/mainCategory?main=pipeline-analysis&&product?=".concat(pa.name)
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, pa.name))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
          onClick: function onClick() {
            return _this2.handleToggle("subCats", paindex);
          },
          className: pa.ulData && "arrow"
        }, "^"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
          className: " ".concat(_this2.state.itemsToShow.subCats.value === paindex && "nav-cats-inner")
        }, pa.ulData && pa.ulData.map(function (ul, ulindex) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
            key: ulindex,
            className: "nav-cats-item-inner"
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
            as: "/category/pipeline-analysis/".concat(pa.name, "/").concat(ul.name),
            href: "/mainCategory?main=pipeline-analysis&&product?=".concat(pa.name, "&&sub").concat(pa.name, "/").concat(ul.name)
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, ul.name))));
        })));
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Our Methodology")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        href: "/contact"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Contact Us")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        href: "/about"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "About Us")))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "nav-icons-box clearfix"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/static/icons/shopping-cart.svg",
        alt: "shopping-cart-icon"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/static/icons/search.svg",
        alt: "search-icon",
        onClick: this.handleClick,
        name: "searchIcon"
      }))), this.state.searchBlock === true && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "nav-searchbar"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_search__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], {
        handleChange: this.handleChange,
        searchInput: this.state.searchInput,
        searchOutput: this.state.searchOutput
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: this.handleClick,
        className: "nav-search-close"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/static/icons/multiply.svg"
      }))));
    }
  }]);

  return NavBar;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

;
/* harmony default export */ __webpack_exports__["a"] = (NavBar);
{
  /* <ul>
                   <li>
                     <Link as={`/category/industry-verticals/agriculture`} href={`/mainCategory?main=industry-verticals&&product=agriculture`}>
                       <a>
                         <p>Agriculture</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals/agriculture/agrochemicals`} href={`/mainCategory?main=industry-verticals&&product=agriculture&&sub=agrochemicals`}>
                           <a>
                             <p>Agrochemicals</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals/agriculture/seed`} href={`/mainCategory?main=industry-verticals&&product=agriculture&&sub=seed`}>
                           <a>
                             <p>Seed</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals/agriculture/seed`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Testing and Technology</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Animal Health</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Feeds</p>
                           </a>
                         </Link>
                       </li>
                       <li><Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                         <a>
                           <p>Veterinary</p>
                         </a>
                       </Link>
                       </li>
                     </ul>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Automotive</p>
                       </a>
                     </Link>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Aviation</p>
                       </a>
                     </Link>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Energy and Utilities</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Power Generation</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li><Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                     <a>
                       <p>Food and Beverages</p>
                     </a>
                   </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Additives</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Beverages</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Processed Food</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Supplements</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Healthcare Services</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Healthcare IT</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Information & Communication</p>
                       </a>
                     </Link>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Medical Devices</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Cardiovascular Devices</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Surgical Devices</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Woundcare Management</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li><Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                     <a>
                       <p>Metals and Mining</p>
                     </a>
                   </Link>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Petrochemicals</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Adhesives & Sealants</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Advanced Materials</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Metals ceramics & Industrial Materials</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Paints and Coatings</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Special and Fine Chemicals</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Speciality Chemicals</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Polymers</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Water Treatment</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li><Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                     <a>
                       <p>Pharmaceuticals</p>
                     </a>
                   </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Oncology</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Ophthalmology</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                 </ul> */
}

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _scss_footer_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(11);
/* harmony import */ var _scss_footer_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_scss_footer_scss__WEBPACK_IMPORTED_MODULE_4__);


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






var FooterSection =
/*#__PURE__*/
function (_Component) {
  _inherits(FooterSection, _Component);

  function FooterSection() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, FooterSection);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(FooterSection)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      formData: {
        preference: "Free Market Updates",
        email: "",
        interestIndustry: "",
        productTitle: "",
        url: ""
      },
      status: "",
      popup: false
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleChangeInput", function (event) {
      var formData = _objectSpread({}, _this.state.formData);

      formData[event.target.name] = event.target.value;

      _this.setState({
        formData: formData
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleSubmit", function (e) {
      e.preventDefault();

      var formData = _objectSpread({}, _this.state.formData);

      formData.productTitle = document.title;
      formData.url = window.location.href;

      _this.setState({
        status: "Submitting Details",
        popup: true,
        formData: formData
      },
      /*#__PURE__*/
      _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var formAPI, newData, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                formAPI = "https://product-page-api.herokuapp.com/api/smtp/footerForm";
                newData = _this.state.formData;
                _context.next = 4;
                return axios__WEBPACK_IMPORTED_MODULE_3___default.a.post(formAPI, newData);

              case 4:
                response = _context.sent;
                console.log(newData);
                console.log(response);

                _this.setState({
                  status: response.data.msg
                });

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      })));
    });

    return _this;
  }

  _createClass(FooterSection, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("footer", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "footer-box"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "container"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "footer"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "section1"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
        src: "/static/Images/DMI-Logo-White.png",
        alt: "Logo-White"
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "DataM Intelligence 4Market Research is a market intelligence platform which gives access to syndicated, customised reports to its clients at one place. As a firm with rich experience in research and consulting across multiple domains we are one stop solution that will cater to the needs of clients in key business areas.")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "section2"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Home"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Our Methodology"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Industry Verticals"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Careers"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Contact Us"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "About Us"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Terms & Conditions"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Privacy Policy"))))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "section3"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h3", null, " Contact "), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Corporate Address"), " ", react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), "1st floor, Phoenix Tech Tower, Plo no: 14/46, Habsidguda, IDA-Uppal, Hyderabad-500039, Telangana"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Email"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), " info@datamintelligence.com"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Phone "), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), "+1 877-441-4866")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "section4"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
        onSubmit: this.handleSubmit
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
        type: "email",
        name: "email",
        autoComplete: "email",
        placeholder: "Email",
        required: true,
        value: this.state.formData.email,
        onChange: this.handleChangeInput
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
        placeholder: "Industry of Interest",
        name: "interestIndustry",
        required: true,
        value: this.state.formData.interestIndustry,
        onChange: this.handleChangeInput
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("select", {
        required: true,
        name: "preference",
        value: this.state.formData.preference,
        onChange: this.handleChangeInput
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
        key: "1",
        value: "Free Market Updates"
      }, "Free Market Updates"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
        key: "2",
        value: "Newsletter"
      }, "Newsletter"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
        key: "3",
        value: "Reports"
      }, "Reports"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
        key: "4",
        value: "Offers"
      }, "Offers")), this.state.popup === true && react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, this.state.status), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", null, "Sign Up"))))))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "montaigne"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "container "
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Copyright \xA9 2019 DataM Intelligence. All Rights Reserved"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Designed and Developed by", react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        href: "www.montaigne.co"
      }, " Montaigne Labs")))));
    }
  }]);

  return FooterSection;
}(react__WEBPACK_IMPORTED_MODULE_1__["Component"]);

/* harmony default export */ __webpack_exports__["a"] = (FooterSection);

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }




var SearchPage =
/*#__PURE__*/
function (_Component) {
  _inherits(SearchPage, _Component);

  function SearchPage() {
    _classCallCheck(this, SearchPage);

    return _possibleConstructorReturn(this, _getPrototypeOf(SearchPage).apply(this, arguments));
  }

  _createClass(SearchPage, [{
    key: "render",
    // state = {
    //     searchInput: "",
    //     searchOutput: [],
    // }
    // static async getInitialProps() {
    //     const url = await fetch(
    //         `https://product-page-api.herokuapp.com/api/category/allData`
    //     );
    //     const data = await url.json();
    //     const newdata = {
    //         data: data
    //     }
    //     return newdata
    // }
    // handleChange = (e) => {
    //     const data = this.props.data;
    //     const options = {
    //         keys: ['Product_Title']
    //     }
    //     let fuse = new Fuse(data, options);
    //     const fuseData = fuse.search(e.target.value);
    //     console.log(fuseData);
    //     this.setState({
    //         searchInput: e.target.value,
    //         searchOutput: fuseData
    //     })
    // }
    value: function render() {
      var _this = this;

      var data = this.props.searchOutput;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "main-search-block"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        type: "search",
        value: this.props.searchInput,
        onChange: function onChange(e) {
          return _this.props.handleChange(e);
        },
        placeholder: "Start typing..."
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "search-result"
      }, data.slice(0, 9).map(function (s, index) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
          as: "/research-reports/".concat(s.slug),
          href: "/product?keyurl=".concat(s.slug)
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: s.ImagePath
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
          key: index
        }, s.Product_Title)));
      })));
    }
  }]);

  return SearchPage;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["a"] = (SearchPage);

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("fuse.js");

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("isomorphic-unfetch");

/***/ }),
/* 10 */
/***/ (function(module, exports) {



/***/ }),
/* 11 */
/***/ (function(module, exports) {



/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_nav__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var _components_footer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





var Layout =
/*#__PURE__*/
function (_Component) {
  _inherits(Layout, _Component);

  function Layout() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Layout);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Layout)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {});

    return _this;
  }

  _createClass(Layout, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_nav__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"], {
        searchProps: this.props.searchProps
      }), this.props.children, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_footer__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null));
    }
  }]);

  return Layout;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["a"] = (Layout);

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),
/* 15 */,
/* 16 */,
/* 17 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(13);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(14);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(8);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _components_layout__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(12);
/* harmony import */ var _components_nav__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(4);
/* harmony import */ var _components_footer__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(5);



function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








 // import "../scss/cart.scss";

var CartPage =
/*#__PURE__*/
function (_Component) {
  _inherits(CartPage, _Component);

  function CartPage() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, CartPage);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(CartPage)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      payInfo: {
        slug: _this.props.slug,
        firstName: "",
        lastName: "",
        companyName: "",
        country: "",
        streetAddress: "",
        townCity: "",
        state: "",
        postCode: "",
        phone: "",
        emailAddress: "",
        orderNote: "",
        itemName: _this.props.productTitle,
        itemPrice: _this.props.selectedPrice,
        sku: "DMM123",
        quantity: 1,
        totalPrice: _this.props.selectedPrice,
        imagePath: _this.props.imagePath
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleChange", function (e) {
      var payInfo = _objectSpread({}, _this.state.payInfo);

      payInfo[e.target.name] = e.target.value;

      if (e.target.name === "quantity") {
        payInfo["totalPrice"] = payInfo["itemPrice"] * e.target.value;
      }

      _this.setState({
        payInfo: payInfo
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleSubmit", function (e) {
      e.preventDefault();
      console.log(_this.state);
      next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push({
        pathname: "/checkout",
        query: {
          slug: _this.state.payInfo.slug,
          firstName: _this.state.payInfo.firstName,
          lastName: _this.state.payInfo.lastName,
          companyName: _this.state.payInfo.companyName,
          country: _this.state.payInfo.country,
          streetAddress: _this.state.payInfo.streetAddress,
          townCity: _this.state.payInfo.townCity,
          state: _this.state.payInfo.state,
          postCode: _this.state.payInfo.postCode,
          phone: _this.state.payInfo.phone,
          emailAddress: _this.state.payInfo.emailAddress,
          orderNote: _this.state.payInfo.orderNote,
          itemName: _this.state.payInfo.itemName,
          itemPrice: _this.state.payInfo.itemPrice,
          sku: _this.state.payInfo.sku,
          quantity: _this.state.payInfo.quantity,
          totalPrice: _this.state.payInfo.totalPrice,
          imagePath: _this.state.payInfo.imagePath
        }
      }, "/checkout");
    });

    return _this;
  }

  _createClass(CartPage, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(next_head__WEBPACK_IMPORTED_MODULE_5___default.a, null, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("meta", {
        name: "viewport",
        content: "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0",
        className: "jsx-3992723784"
      }), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("meta", {
        charSet: "utf-8",
        className: "jsx-3992723784"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784" + " " + "navbar"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_components_nav__WEBPACK_IMPORTED_MODULE_8__[/* default */ "a"], null)), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        id: "cart",
        className: "jsx-3992723784"
      }, this.props.slug === undefined && this.props.slug !== "" && react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("h1", {
        className: "jsx-3992723784"
      }, "There are no items in your CART!"), this.props.slug !== undefined && react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_2___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("h1", {
        className: "jsx-3992723784"
      }, "CART"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784" + " " + "cart-section"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784" + " " + "product-container"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784" + " " + "product-meta"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("img", {
        src: this.props.imagePath,
        alt: "",
        className: "jsx-3992723784"
      }), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_4___default.a, {
        as: "/research-reports/".concat(this.state.payInfo.slug),
        href: "/product?keyurl=".concat(this.state.payInfo.slug)
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("a", {
        className: "jsx-3992723784"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("h4", {
        className: "jsx-3992723784"
      }, this.state.payInfo.itemName)))), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784" + " " + "price-container"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784" + " " + "form-group"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-3992723784"
      }, "Quantity"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "number",
        name: "quantity",
        value: this.state.payInfo.quantity,
        onChange: this.handleChange,
        className: "jsx-3992723784"
      }), " x $", this.state.payInfo.itemPrice), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784" + " " + "price"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("h4", {
        className: "jsx-3992723784"
      }, "$", this.state.payInfo.totalPrice))))), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784" + " " + "form-section"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("h2", {
        className: "jsx-3992723784"
      }, "Billing Details"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("form", {
        onSubmit: this.handleSubmit,
        className: "jsx-3992723784"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784" + " " + "form-group"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-3992723784"
      }, "First Name:"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "text",
        name: "firstName",
        value: this.state.payInfo.firstName,
        onChange: this.handleChange,
        className: "jsx-3992723784"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784" + " " + "form-group"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-3992723784"
      }, "Last Name:"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "text",
        name: "lastName",
        value: this.state.payInfo.lastName,
        onChange: this.handleChange,
        className: "jsx-3992723784"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784" + " " + "form-group"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-3992723784"
      }, "Company :"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "text",
        name: "companyName",
        value: this.state.payInfo.companyName,
        onChange: this.handleChange,
        className: "jsx-3992723784"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784" + " " + "form-group"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-3992723784"
      }, "Country:"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "text",
        name: "country",
        value: this.state.payInfo.country,
        onChange: this.handleChange,
        className: "jsx-3992723784"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784" + " " + "form-group"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-3992723784"
      }, "Street Address:"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "text",
        name: "streetAddress",
        value: this.state.payInfo.streetAddress,
        onChange: this.handleChange,
        className: "jsx-3992723784"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784" + " " + "form-group"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-3992723784"
      }, "Town/City:"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "text",
        name: "townCity",
        value: this.state.payInfo.townCity,
        onChange: this.handleChange,
        className: "jsx-3992723784"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784" + " " + "form-group"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-3992723784"
      }, "State:"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "text",
        name: "state",
        value: this.state.payInfo.state,
        onChange: this.handleChange,
        className: "jsx-3992723784"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784" + " " + "form-group"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-3992723784"
      }, "Postal Code:"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "number",
        name: "postCode",
        value: this.state.payInfo.postCode,
        onChange: this.handleChange,
        className: "jsx-3992723784"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784" + " " + "form-group"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-3992723784"
      }, "Phone:"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "number",
        name: "phone",
        value: this.state.payInfo.phone,
        onChange: this.handleChange,
        className: "jsx-3992723784"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784" + " " + "form-group"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-3992723784"
      }, "Email:"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "email",
        name: "emailAddress",
        value: this.state.payInfo.emailAddress,
        onChange: this.handleChange,
        className: "jsx-3992723784"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784" + " " + "form-group-100 "
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-3992723784"
      }, "Note:"), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "text",
        name: "orderNote",
        value: this.state.payInfo.orderNote,
        onChange: this.handleChange,
        className: "jsx-3992723784"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-3992723784" + " " + "form-group-100"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("button", {
        className: "jsx-3992723784"
      }, "Checkout")))))), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_components_footer__WEBPACK_IMPORTED_MODULE_9__[/* default */ "a"], null), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_1___default.a, {
        styleId: "3992723784",
        css: [".navbar.jsx-3992723784{height:40rem;background-image:url('/static/Images/join-us-bg.jpg');background-repeat:no-repeat;background-position:center bottom;background-size:cover;}", "#cart.jsx-3992723784{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:80px 0px;}", "#cart.jsx-3992723784 h1.jsx-3992723784{color:#f60;font-family:'bebas neue';font-size:5.4rem;line-height:65px;-webkit-letter-spacing:-0.07px;-moz-letter-spacing:-0.07px;-ms-letter-spacing:-0.07px;letter-spacing:-0.07px;text-align:center;padding-bottom:40px;width:100%;}", "#cart.jsx-3992723784 .cart-section.jsx-3992723784,#cart.jsx-3992723784 .form-section.jsx-3992723784{width:50%;-webkit-flex:1;-ms-flex:1;flex:1;}", "#cart.jsx-3992723784 .cart-section.jsx-3992723784{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-box-pack:start;-webkit-justify-content:flex-start;-ms-flex-pack:start;justify-content:flex-start;-webkit-align-items:flex-end;-webkit-box-align:flex-end;-ms-flex-align:flex-end;align-items:flex-end;max-width:500px;padding:20px 40px;box-sizing:border-box;border-right:1px solid #ccc;overflow-y:scroll;}", "#cart.jsx-3992723784 .cart-section.jsx-3992723784>div.product-container.jsx-3992723784{margin:10px 0px;}", "#cart.jsx-3992723784 .cart-section.jsx-3992723784 .product-container.jsx-3992723784{width:100%;background:#fff;border:1px solid #ccc;border-radius:5px;box-shadow:0px 4px 4px rgba(0,0,0,0.25);display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;min-height:120px;padding:10px;box-sizing:border-box;}", "#cart.jsx-3992723784 .cart-section.jsx-3992723784 .product-container.jsx-3992723784 .product-meta.jsx-3992723784{-webkit-flex:3;-ms-flex:3;flex:3;height:100px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:space-evenly;-webkit-justify-content:space-evenly;-ms-flex-pack:space-evenly;justify-content:space-evenly;}", "#cart.jsx-3992723784 .cart-section.jsx-3992723784 .product-container.jsx-3992723784 .product-meta.jsx-3992723784 h4.jsx-3992723784{font-size:1.4rem;padding-left:5px;}", "#cart.jsx-3992723784 .cart-section.jsx-3992723784 .product-container.jsx-3992723784 .product-meta.jsx-3992723784 img.jsx-3992723784{max-height:100%;padding-right:5px;}", "#cart.jsx-3992723784 .cart-section.jsx-3992723784 .product-container.jsx-3992723784 .price-container.jsx-3992723784{padding:0px 10px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between;-webkit-align-items:flex-end;-webkit-box-align:flex-end;-ms-flex-align:flex-end;align-items:flex-end;-webkit-flex:1;-ms-flex:1;flex:1;}", "#cart.jsx-3992723784 .cart-section.jsx-3992723784 .product-container.jsx-3992723784 .price-container.jsx-3992723784 .form-group.jsx-3992723784{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;box-sizing:border-box;}", "#cart.jsx-3992723784 .cart-section.jsx-3992723784 .product-container.jsx-3992723784 .price-container.jsx-3992723784 .form-group.jsx-3992723784 label.jsx-3992723784{font-weight:bold;display:block;color:#2e2e2e;}", "#cart.jsx-3992723784 .cart-section.jsx-3992723784 .product-container.jsx-3992723784 .price-container.jsx-3992723784 .form-group.jsx-3992723784 input.jsx-3992723784{background:#e8e8e8;width:100%;max-height:30px;padding:10px 0px;padding-left:10px;box-sizing:border-box;}", "#cart.jsx-3992723784 .cart-section.jsx-3992723784 .product-container.jsx-3992723784 .price-container.jsx-3992723784 h4.jsx-3992723784{width:100%;font-size:1.8rem;}", "#cart.jsx-3992723784 .form-section.jsx-3992723784{padding:20px 40px;max-width:420px;}", "#cart.jsx-3992723784 .form-section.jsx-3992723784 h2.jsx-3992723784{text-align:center;font-size:2.5rem;padding-bottom:20px;}", "#cart.jsx-3992723784 .form-section.jsx-3992723784 form.jsx-3992723784{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;-webkit-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap;font-family:'Muli',sans-serif;}", "#cart.jsx-3992723784 .form-section.jsx-3992723784 form.jsx-3992723784 .form-group.jsx-3992723784{width:50%;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;box-sizing:border-box;}", "#cart.jsx-3992723784 .form-section.jsx-3992723784 form.jsx-3992723784 .form-group-100.jsx-3992723784{width:100%;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;box-sizing:border-box;}", "#cart.jsx-3992723784 .form-section.jsx-3992723784 form.jsx-3992723784 label.jsx-3992723784{font-weight:bold;display:block;color:#2e2e2e;}", "#cart.jsx-3992723784 .form-section.jsx-3992723784 form.jsx-3992723784 input.jsx-3992723784{background:#e8e8e8;width:100%;padding:10px 0px;padding-left:10px;margin-top:5px;margin-bottom:20px;box-sizing:border-box;}", "#cart.jsx-3992723784 .form-section.jsx-3992723784 form.jsx-3992723784 button.jsx-3992723784{background:#2e2e2e;padding:10px 20px;color:#fff;font-family:'Muli',sans-serif;}", "#cart.jsx-3992723784 .form-section.jsx-3992723784 form.jsx-3992723784>div.form-group.jsx-3992723784:nth-child(odd){padding-right:20px;}", "#cart.jsx-3992723784 .form-section.jsx-3992723784 form.jsx-3992723784>div.form-group.jsx-3992723784:nth-child(even){padding-left:20px;}", "@media (min-width.jsx-3992723784:320px) and (max-width.jsx-3992723784:880px) #cart{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;}", "@media (min-width.jsx-3992723784:320px) and (max-width.jsx-3992723784:880px) #cart h1.jsx-3992723784{padding-bottom:20px;}", "@media (min-width.jsx-3992723784:320px) and (max-width.jsx-3992723784:880px) #cart .cart-section.jsx-3992723784,@media (min-width.jsx-3992723784:320px) and (max-width.jsx-3992723784:880px) #cart .form-section.jsx-3992723784{width:100%;}", "@media (min-width.jsx-3992723784:320px) and (max-width.jsx-3992723784:880px) #cart .cart-section.jsx-3992723784{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;border:none;border-bottom:1px solid #ccc;max-width:100%;}", "@media (min-width.jsx-3992723784:320px) and (max-width.jsx-3992723784:880px) #cart .cart-section.jsx-3992723784 .product-container.jsx-3992723784{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;width:100%;min-height:300px;padding:10px;box-sizing:border-box;}", "@media (min-width.jsx-3992723784:320px) and (max-width.jsx-3992723784:880px) #cart .cart-section.jsx-3992723784>div.product-container.jsx-3992723784{margin:10px 0px;}", "@media (min-width.jsx-3992723784:320px) and (max-width.jsx-3992723784:880px) #cart .cart-section.jsx-3992723784 .product-container.jsx-3992723784 .product-meta.jsx-3992723784{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;}", "@media (min-width.jsx-3992723784:320px) and (max-width.jsx-3992723784:880px) #cart .cart-section.jsx-3992723784 .product-container.jsx-3992723784 .product-meta.jsx-3992723784 img.jsx-3992723784{margin-bottom:10px;}", "@media (min-width.jsx-3992723784:320px) and (max-width.jsx-3992723784:880px) #cart .cart-section.jsx-3992723784 .product-container.jsx-3992723784 .product-meta.jsx-3992723784 h4.jsx-3992723784{text-align:center;}", "@media (min-width.jsx-3992723784:320px) and (max-width.jsx-3992723784:880px) #cart .cart-section.jsx-3992723784 .product-container.jsx-3992723784 .price-container.jsx-3992723784{padding:0px;height:auto;width:100%;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-flex:1;-ms-flex:1;flex:1;}", "@media (min-width.jsx-3992723784:320px) and (max-width.jsx-3992723784:880px) #cart .cart-section.jsx-3992723784 .product-container.jsx-3992723784 .price-container.jsx-3992723784 label.jsx-3992723784{margin-bottom:6px;}", "@media (min-width.jsx-3992723784:320px) and (max-width.jsx-3992723784:880px) #cart .cart-section.jsx-3992723784 .product-container.jsx-3992723784 .price-container.jsx-3992723784 input.jsx-3992723784{margin-bottom:10px;}", "@media (min-width.jsx-3992723784:320px) and (max-width.jsx-3992723784:880px) #cart .form-section.jsx-3992723784{box-sizing:border-box;max-width:100%;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap;}", "@media (min-width.jsx-3992723784:320px) and (max-width.jsx-3992723784:880px) #cart .form-section.jsx-3992723784 form.jsx-3992723784{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;max-width:80%;margin:0px;}", "@media (min-width.jsx-3992723784:320px) and (max-width.jsx-3992723784:880px) #cart .form-section.jsx-3992723784 form.jsx-3992723784 .form-group.jsx-3992723784{width:100%;}", "@media (min-width.jsx-3992723784:320px) and (max-width.jsx-3992723784:880px) #cart .form-section.jsx-3992723784 form.jsx-3992723784>div.form-group.jsx-3992723784:nth-child(odd){padding-right:0px;}", "@media (min-width.jsx-3992723784:320px) and (max-width.jsx-3992723784:880px) #cart .form-section.jsx-3992723784 form.jsx-3992723784>div.form-group.jsx-3992723784:nth-child(even){padding-left:0px;}"]
      }));
    }
  }], [{
    key: "getInitialProps",
    value: function () {
      var _getInitialProps = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(_ref) {
        var req, res, query;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                req = _ref.req, res = _ref.res, query = _ref.query;
                return _context.abrupt("return", query);

              case 2:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function getInitialProps(_x) {
        return _getInitialProps.apply(this, arguments);
      }

      return getInitialProps;
    }() // componentDidUpdate(oldProps) {
    //     const newProps = this.props
    //     if (oldProps !== newProps) {
    //         this.setState({
    //             slug: this.props.slug,
    //             // firstName: "",
    //             // lastName: "",
    //             // companyName: "",
    //             // country: "",
    //             // streetAddress: "",
    //             // townCity: "",
    //             // state: "",
    //             // postCode: "",
    //             // phone: "",
    //             // emailAddress: "",
    //             // orderNote: "",
    //             itemName: this.props.productTitle,
    //             itemPrice: this.props.selectedPrice,
    //             sku: "DMM123",
    //             quantity: 1,
    //             totalPrice: this.props.selectedPrice
    //         });
    //     }
    // }

  }]);

  return CartPage;
}(react__WEBPACK_IMPORTED_MODULE_2__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(next_router__WEBPACK_IMPORTED_MODULE_3__["withRouter"])(CartPage));

/***/ }),
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */,
/* 39 */,
/* 40 */,
/* 41 */,
/* 42 */,
/* 43 */,
/* 44 */,
/* 45 */,
/* 46 */,
/* 47 */,
/* 48 */,
/* 49 */,
/* 50 */,
/* 51 */,
/* 52 */,
/* 53 */,
/* 54 */,
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(84);


/***/ }),
/* 56 */
/***/ (function(module, exports) {



/***/ }),
/* 57 */
/***/ (function(module, exports) {



/***/ }),
/* 58 */
/***/ (function(module, exports) {

module.exports = require("constants");

/***/ }),
/* 59 */
/***/ (function(module, exports) {



/***/ }),
/* 60 */
/***/ (function(module, exports) {



/***/ }),
/* 61 */
/***/ (function(module, exports) {



/***/ }),
/* 62 */
/***/ (function(module, exports) {



/***/ }),
/* 63 */
/***/ (function(module, exports) {



/***/ }),
/* 64 */
/***/ (function(module, exports) {



/***/ }),
/* 65 */,
/* 66 */,
/* 67 */,
/* 68 */,
/* 69 */,
/* 70 */,
/* 71 */,
/* 72 */,
/* 73 */,
/* 74 */,
/* 75 */,
/* 76 */,
/* 77 */,
/* 78 */,
/* 79 */,
/* 80 */,
/* 81 */,
/* 82 */,
/* 83 */,
/* 84 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: external "@babel/runtime/regenerator"
var regenerator_ = __webpack_require__(2);
var regenerator_default = /*#__PURE__*/__webpack_require__.n(regenerator_);

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(0);
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(8);
var head_default = /*#__PURE__*/__webpack_require__.n(head_);

// EXTERNAL MODULE: external "isomorphic-unfetch"
var external_isomorphic_unfetch_ = __webpack_require__(9);
var external_isomorphic_unfetch_default = /*#__PURE__*/__webpack_require__.n(external_isomorphic_unfetch_);

// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(14);
var router_default = /*#__PURE__*/__webpack_require__.n(router_);

// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(3);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);

// EXTERNAL MODULE: external "next/link"
var link_ = __webpack_require__(1);
var link_default = /*#__PURE__*/__webpack_require__.n(link_);

// EXTERNAL MODULE: ./scss/buy-report.scss
var buy_report = __webpack_require__(56);

// CONCATENATED MODULE: ./components/buy-report.jsx
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





var buy_report_BuyReport =
/*#__PURE__*/
function (_Component) {
  _inherits(BuyReport, _Component);

  function BuyReport() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, BuyReport);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(BuyReport)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      template: _this.props.template,
      Prices: _this.props.Prices
    });

    return _this;
  }

  _createClass(BuyReport, [{
    key: "render",
    value: function render() {
      var Price = this.state.Prices;
      return external_react_default.a.createElement("div", {
        className: "buy-report-box"
      }, external_react_default.a.createElement("h4", null, "Buy This Report"), this.state.template === "default" && external_react_default.a.createElement("form", {
        className: "buy-report-form",
        onSubmit: this.props.handleSubmit
      }, external_react_default.a.createElement("label", null, external_react_default.a.createElement("span", {
        className: "radio-icon ".concat(this.props.value === 1 && "active-icon")
      }, external_react_default.a.createElement("input", _defineProperty({
        type: "radio",
        name: "default-prices",
        onChange: this.handleChange,
        value: "".concat(Price.Single),
        checked: this.props.value === 1
      }, "onChange", this.props.handleRadioButton(1)))), "Single User", external_react_default.a.createElement("span", {
        className: "pricetags ".concat(this.props.value === 1 && "active")
      }, "$".concat(Price.Single))), external_react_default.a.createElement("hr", null), external_react_default.a.createElement("label", null, external_react_default.a.createElement("span", {
        className: "radio-icon ".concat(this.props.value === 2 && "active-icon")
      }, external_react_default.a.createElement("input", _defineProperty({
        type: "radio",
        name: "default-prices",
        onChange: this.handleChange,
        value: "".concat(Price.Multiple),
        checked: this.props.value === 2
      }, "onChange", this.props.handleRadioButton(2)))), "Multiple User", external_react_default.a.createElement("span", {
        className: "pricetags ".concat(this.props.value === 2 && "active")
      }, "$".concat(Price.Multiple))), external_react_default.a.createElement("hr", null), external_react_default.a.createElement("label", null, external_react_default.a.createElement("span", {
        className: "radio-icon ".concat(this.props.value === 3 && "active-icon")
      }, external_react_default.a.createElement("input", _defineProperty({
        type: "radio",
        name: "default-prices",
        onChange: this.handleChange,
        value: "".concat(Price.Enterprise),
        checked: this.props.value === 3
      }, "onChange", this.props.handleRadioButton(3)))), "Enterprise User", external_react_default.a.createElement("span", {
        className: "pricetags ".concat(this.props.value === 3 && "active")
      }, "$".concat(Price.Enterprise))), external_react_default.a.createElement("button", {
        className: "buy-report-btn"
      }, "ADD TO CART")), this.state.template === "offer-20%" && external_react_default.a.createElement("form", {
        className: "buy-report-form",
        onSubmit: this.props.handleSubmit
      }, external_react_default.a.createElement("label", null, external_react_default.a.createElement("span", {
        className: "radio-icon ".concat(this.props.value === 1 && "active-icon")
      }, external_react_default.a.createElement("input", _defineProperty({
        type: "radio",
        name: "default-prices",
        onChange: this.handleChange,
        value: "".concat(Price.Single - 20 * Price.Single / 100),
        checked: this.props.value === 1
      }, "onChange", this.props.handleRadioButton(1)))), "Single User", external_react_default.a.createElement("span", {
        className: "pricetags ".concat(this.props.value === 1 && "active")
      }, external_react_default.a.createElement("strike", null, "$".concat(Price.Single)), "$".concat(Price.Single - 20 * Price.Single / 100))), external_react_default.a.createElement("hr", null), external_react_default.a.createElement("label", null, external_react_default.a.createElement("span", {
        className: "radio-icon ".concat(this.props.value === 2 && "active-icon")
      }, external_react_default.a.createElement("input", _defineProperty({
        type: "radio",
        name: "default-prices",
        onChange: this.handleChange,
        value: "".concat(Price.Multiple - 20 * Price.Multiple / 100),
        checked: this.props.value === 2
      }, "onChange", this.props.handleRadioButton(2)))), "Multiple User", external_react_default.a.createElement("span", {
        className: "pricetags ".concat(this.props.value === 2 && "active")
      }, external_react_default.a.createElement("strike", null, "$".concat(Price.Multiple)), "$".concat(Price.Multiple - 20 * Price.Multiple / 100))), external_react_default.a.createElement("hr", null), external_react_default.a.createElement("label", null, external_react_default.a.createElement("span", {
        className: "radio-icon ".concat(this.props.value === 3 && "active-icon")
      }, external_react_default.a.createElement("input", _defineProperty({
        type: "radio",
        name: "default-prices",
        onChange: this.handleChange,
        value: "".concat(Price.Enterprise - 20 * Price.Enterprise / 100),
        checked: this.props.value === 3
      }, "onChange", this.props.handleRadioButton(3)))), "Enterprise User", external_react_default.a.createElement("span", {
        className: "pricetags ".concat(this.props.value === 3 && "active")
      }, external_react_default.a.createElement("strike", null, "$".concat(Price.Enterprise)), "$".concat(Price.Enterprise - 20 * Price.Enterprise / 100))), external_react_default.a.createElement("button", {
        className: "buy-report-btn"
      }, "ADD TO CART")));
    }
  }]);

  return BuyReport;
}(external_react_["Component"]);

/* harmony default export */ var components_buy_report = (buy_report_BuyReport);
// EXTERNAL MODULE: ./scss/download-sample-form.scss
var download_sample_form = __webpack_require__(57);

// EXTERNAL MODULE: external "constants"
var external_constants_ = __webpack_require__(58);

// CONCATENATED MODULE: ./components/download-sample.jsx


function download_sample_typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { download_sample_typeof = function _typeof(obj) { return typeof obj; }; } else { download_sample_typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return download_sample_typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { download_sample_defineProperty(target, key, source[key]); }); } return target; }

function download_sample_classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function download_sample_defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function download_sample_createClass(Constructor, protoProps, staticProps) { if (protoProps) download_sample_defineProperties(Constructor.prototype, protoProps); if (staticProps) download_sample_defineProperties(Constructor, staticProps); return Constructor; }

function download_sample_possibleConstructorReturn(self, call) { if (call && (download_sample_typeof(call) === "object" || typeof call === "function")) { return call; } return download_sample_assertThisInitialized(self); }

function download_sample_getPrototypeOf(o) { download_sample_getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return download_sample_getPrototypeOf(o); }

function download_sample_inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) download_sample_setPrototypeOf(subClass, superClass); }

function download_sample_setPrototypeOf(o, p) { download_sample_setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return download_sample_setPrototypeOf(o, p); }

function download_sample_assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function download_sample_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






var download_sample_DownloadSample =
/*#__PURE__*/
function (_Component) {
  download_sample_inherits(DownloadSample, _Component);

  function DownloadSample() {
    var _getPrototypeOf2;

    var _this;

    download_sample_classCallCheck(this, DownloadSample);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = download_sample_possibleConstructorReturn(this, (_getPrototypeOf2 = download_sample_getPrototypeOf(DownloadSample)).call.apply(_getPrototypeOf2, [this].concat(args)));

    download_sample_defineProperty(download_sample_assertThisInitialized(download_sample_assertThisInitialized(_this)), "state", {
      formData: {
        name: "",
        email: "",
        phone: "",
        company: "",
        designation: "",
        message: "",
        productTitle: "",
        url: ""
      },
      status: "",
      popup: false
    });

    download_sample_defineProperty(download_sample_assertThisInitialized(download_sample_assertThisInitialized(_this)), "handleChangeInput", function (event) {
      var formData = _objectSpread({}, _this.state.formData);

      formData[event.target.name] = event.target.value;

      _this.setState({
        formData: formData
      });
    });

    download_sample_defineProperty(download_sample_assertThisInitialized(download_sample_assertThisInitialized(_this)), "handleSubmit", function (e) {
      e.preventDefault();

      var formData = _objectSpread({}, _this.state.formData);

      formData.productTitle = document.title;
      formData.url = window.location.href;

      _this.setState({
        status: "Submitting Details",
        popup: true,
        formData: formData
      },
      /*#__PURE__*/
      _asyncToGenerator(
      /*#__PURE__*/
      regenerator_default.a.mark(function _callee() {
        var formAPI, newData, response;
        return regenerator_default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                formAPI = "https://product-page-api.herokuapp.com/api/smtp/downloadSample";
                newData = _this.state.formData;
                _context.next = 4;
                return external_axios_default.a.post(formAPI, newData);

              case 4:
                response = _context.sent;
                console.log(newData);
                console.log(response);

                _this.setState({
                  status: response.data.msg
                });

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      })));
    });

    return _this;
  }

  download_sample_createClass(DownloadSample, [{
    key: "render",
    value: function render() {
      var _React$createElement;

      return external_react_default.a.createElement("div", {
        className: "ds-form-box",
        style: {
          transition: "max-height .5s ease-in-out",
          maxHeight: this.props.height,
          overflow: "hidden"
        }
      }, external_react_default.a.createElement("div", {
        className: "container"
      }, external_react_default.a.createElement("h2", null, "Guide your sample to inbox"), external_react_default.a.createElement("button", {
        className: "close-form",
        onClick: this.props.onClose
      }, "X"), external_react_default.a.createElement("form", {
        className: "ds-form clearfix",
        onSubmit: this.handleSubmit
      }, external_react_default.a.createElement("input", {
        name: "name",
        placeholder: "name",
        required: true,
        className: "normal",
        value: this.state.formData.name,
        onChange: this.handleChangeInput
      }), external_react_default.a.createElement("input", {
        name: "phone",
        placeholder: "phone",
        type: "number",
        required: true,
        className: "normal",
        value: this.state.formData.phone,
        onChange: this.handleChangeInput
      }), external_react_default.a.createElement("input", {
        required: true,
        autoComplete: "email",
        type: "email",
        name: "email",
        placeholder: "email",
        className: "normal",
        value: this.state.formData.email,
        onChange: this.handleChangeInput
      }), external_react_default.a.createElement("input", {
        name: "company",
        placeholder: "company",
        required: true,
        className: "normal",
        value: this.state.formData.company,
        onChange: this.handleChangeInput
      }), external_react_default.a.createElement("input", {
        name: "designation",
        placeholder: "designation",
        required: true,
        className: "fullwidth",
        value: this.state.formData.designation,
        onChange: this.handleChangeInput
      }), external_react_default.a.createElement("textarea", (_React$createElement = {
        name: "message",
        type: "text",
        required: true,
        value: "Message",
        className: "fullwidth"
      }, download_sample_defineProperty(_React$createElement, "value", this.state.formData.message), download_sample_defineProperty(_React$createElement, "onChange", this.handleChangeInput), _React$createElement), "Your Message"), external_react_default.a.createElement("button", null, "Submit"), this.state.popup === true && external_react_default.a.createElement("p", {
        style: {
          fontSize: "2rem"
        }
      }, this.state.status))));
    }
  }]);

  return DownloadSample;
}(external_react_["Component"]);

/* harmony default export */ var download_sample = (download_sample_DownloadSample);
// EXTERNAL MODULE: ./scss/product-main-sec.scss
var product_main_sec = __webpack_require__(59);

// CONCATENATED MODULE: ./components/product-main.jsx
function product_main_typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { product_main_typeof = function _typeof(obj) { return typeof obj; }; } else { product_main_typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return product_main_typeof(obj); }

function product_main_classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function product_main_defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function product_main_createClass(Constructor, protoProps, staticProps) { if (protoProps) product_main_defineProperties(Constructor.prototype, protoProps); if (staticProps) product_main_defineProperties(Constructor, staticProps); return Constructor; }

function product_main_possibleConstructorReturn(self, call) { if (call && (product_main_typeof(call) === "object" || typeof call === "function")) { return call; } return product_main_assertThisInitialized(self); }

function product_main_getPrototypeOf(o) { product_main_getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return product_main_getPrototypeOf(o); }

function product_main_inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) product_main_setPrototypeOf(subClass, superClass); }

function product_main_setPrototypeOf(o, p) { product_main_setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return product_main_setPrototypeOf(o, p); }

function product_main_assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function product_main_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



 // let countProps = this.props.SKU.count;
// let precountValue = preCounter(countProps);
// function preCounter(countProps) {
//   let countLength = countProps.length;
//   let precountValue = "";
//   for (var i = countLength; i < 4; i++) {
//     precountValue += "0";
//   }
//   return precountValue
// }

var product_main_ProductTitleSection =
/*#__PURE__*/
function (_Component) {
  product_main_inherits(ProductTitleSection, _Component);

  function ProductTitleSection() {
    var _getPrototypeOf2;

    var _this;

    product_main_classCallCheck(this, ProductTitleSection);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = product_main_possibleConstructorReturn(this, (_getPrototypeOf2 = product_main_getPrototypeOf(ProductTitleSection)).call.apply(_getPrototypeOf2, [this].concat(args)));

    product_main_defineProperty(product_main_assertThisInitialized(product_main_assertThisInitialized(_this)), "state", {
      height: 0,
      countProps: _this.props.SKU.count,
      precountValue: ""
    });

    product_main_defineProperty(product_main_assertThisInitialized(product_main_assertThisInitialized(_this)), "handleOpen", function () {
      _this.setState({
        height: "100rem"
      });
    });

    product_main_defineProperty(product_main_assertThisInitialized(product_main_assertThisInitialized(_this)), "handleClose", function () {
      _this.setState({
        height: 0
      });
    });

    return _this;
  }

  product_main_createClass(ProductTitleSection, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var countLength = this.state.countProps;
      console.log(countLength);
      var precount = "";

      for (var i = countLength; i < 4; i++) {
        precount += "0";
      }

      this.setState({
        precountValue: precount
      });
    }
  }, {
    key: "render",
    value: function render() {
      return external_react_default.a.createElement(external_react_default.a.Fragment, null, external_react_default.a.createElement("div", {
        className: "clearfix product-main-sec-box container"
      }, external_react_default.a.createElement("div", {
        className: "product-img-box"
      }, external_react_default.a.createElement("img", {
        src: this.props.ImagePath
      })), external_react_default.a.createElement("div", {
        className: "product-title-box"
      }, external_react_default.a.createElement("p", {
        className: "Date-and-Format"
      }, "SKU: D", this.props.SKU.category, this.state.precountValue, this.props.SKU.count, " | Published On: ", this.props.PublishedDate, " | Available Formats", " ", external_react_default.a.createElement("img", {
        src: "/static/Images/available-formats.png"
      }), " "), external_react_default.a.createElement("h1", null, this.props.Title)), external_react_default.a.createElement("div", {
        className: "product-button-box"
      }, external_react_default.a.createElement("button", {
        onClick: this.handleOpen,
        className: "download-sample-btn"
      }, "Download Sample"), external_react_default.a.createElement("button", {
        className: "product-compass-btn"
      }, " ", external_react_default.a.createElement("img", {
        src: "/static/Images/compass logo-white.png"
      })))), external_react_default.a.createElement("div", {
        className: "product-form-box"
      }, external_react_default.a.createElement(download_sample, {
        onClose: this.handleClose,
        height: this.state.height
      })));
    }
  }]);

  return ProductTitleSection;
}(external_react_["Component"]);

/* harmony default export */ var product_main = (product_main_ProductTitleSection);
// EXTERNAL MODULE: ./scss/related-reports.scss
var related_reports = __webpack_require__(60);

// CONCATENATED MODULE: ./components/related-reports-sec.jsx




var related_reports_sec_RelatedReportSec = function RelatedReportSec(props) {
  var listItems = props.relatedReports.map(function (report, index) {
    return external_react_default.a.createElement("li", {
      key: index,
      className: "related-reports-item"
    }, external_react_default.a.createElement("img", {
      src: report.ImagePath,
      className: "related-reports-img"
    }), external_react_default.a.createElement(link_default.a, {
      as: "/research-reports/".concat(report.slug),
      href: "/product?keyurl=".concat(report.slug)
    }, external_react_default.a.createElement("a", null, report.Product_Title)));
  });
  return external_react_default.a.createElement("div", {
    className: "related-reports-box"
  }, external_react_default.a.createElement("h3", null, "People also searched for"), external_react_default.a.createElement("ul", {
    className: "related-reports-list"
  }, listItems));
};

/* harmony default export */ var related_reports_sec = (related_reports_sec_RelatedReportSec);
// EXTERNAL MODULE: ./scss/rd-sec.scss
var rd_sec = __webpack_require__(61);

// CONCATENATED MODULE: ./components/report-data-sec.jsx
function report_data_sec_typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { report_data_sec_typeof = function _typeof(obj) { return typeof obj; }; } else { report_data_sec_typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return report_data_sec_typeof(obj); }

function report_data_sec_classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function report_data_sec_defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function report_data_sec_createClass(Constructor, protoProps, staticProps) { if (protoProps) report_data_sec_defineProperties(Constructor.prototype, protoProps); if (staticProps) report_data_sec_defineProperties(Constructor, staticProps); return Constructor; }

function report_data_sec_possibleConstructorReturn(self, call) { if (call && (report_data_sec_typeof(call) === "object" || typeof call === "function")) { return call; } return report_data_sec_assertThisInitialized(self); }

function report_data_sec_getPrototypeOf(o) { report_data_sec_getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return report_data_sec_getPrototypeOf(o); }

function report_data_sec_inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) report_data_sec_setPrototypeOf(subClass, superClass); }

function report_data_sec_setPrototypeOf(o, p) { report_data_sec_setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return report_data_sec_setPrototypeOf(o, p); }

function report_data_sec_assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function report_data_sec_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




var report_data_sec_RDSection =
/*#__PURE__*/
function (_Component) {
  report_data_sec_inherits(RDSection, _Component);

  function RDSection() {
    var _getPrototypeOf2;

    var _this;

    report_data_sec_classCallCheck(this, RDSection);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = report_data_sec_possibleConstructorReturn(this, (_getPrototypeOf2 = report_data_sec_getPrototypeOf(RDSection)).call.apply(_getPrototypeOf2, [this].concat(args)));

    report_data_sec_defineProperty(report_data_sec_assertThisInitialized(report_data_sec_assertThisInitialized(_this)), "state", {
      value: 0
    });

    report_data_sec_defineProperty(report_data_sec_assertThisInitialized(report_data_sec_assertThisInitialized(_this)), "handleChange", function (index) {
      _this.setState({
        value: index
      });
    });

    return _this;
  }

  report_data_sec_createClass(RDSection, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var TabDatas = this.props.TabsData; // const displayTabs = TabDatas.map((shit, index) => (
      //   <Tab label={shit.TabName} key={index} />
      // ));

      return external_react_default.a.createElement("div", {
        className: "rd-box"
      }, external_react_default.a.createElement("div", {
        className: "rd-tab-box clearfix"
      }, this.props.TabsData.map(function (TabData, index) {
        return external_react_default.a.createElement("button", {
          key: index,
          onClick: function onClick() {
            return _this2.handleChange(index);
          },
          className: "".concat(_this2.state.value === index && "active")
        }, TabData.TabName);
      })), TabDatas.map(function (TabData, index) {
        return external_react_default.a.createElement("div", {
          dangerouslySetInnerHTML: {
            __html: TabData.TabText
          },
          key: index,
          className: "rd-content-box ".concat(_this2.state.value === index && "active-content")
        });
      }));
    }
  }]);

  return RDSection;
}(external_react_["Component"]);

/* harmony default export */ var report_data_sec = (report_data_sec_RDSection);
{
  /* {Tabs.map((Tab, index) => {
            {
              this.state.value === index && (
                <TabContainer>
                  <div
                    dangerouslySetInnerHTML={{ __html: Tab[index].TabText }}
                  />
                </TabContainer>
              );
            }
          })} */
}
// EXTERNAL MODULE: ./scss/bundle-report.scss
var bundle_report = __webpack_require__(62);

// CONCATENATED MODULE: ./components/bundle-report.jsx


function bundle_report_typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { bundle_report_typeof = function _typeof(obj) { return typeof obj; }; } else { bundle_report_typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return bundle_report_typeof(obj); }

function bundle_report_asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function bundle_report_asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { bundle_report_asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { bundle_report_asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function bundle_report_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { bundle_report_defineProperty(target, key, source[key]); }); } return target; }

function bundle_report_classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function bundle_report_defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function bundle_report_createClass(Constructor, protoProps, staticProps) { if (protoProps) bundle_report_defineProperties(Constructor.prototype, protoProps); if (staticProps) bundle_report_defineProperties(Constructor, staticProps); return Constructor; }

function bundle_report_possibleConstructorReturn(self, call) { if (call && (bundle_report_typeof(call) === "object" || typeof call === "function")) { return call; } return bundle_report_assertThisInitialized(self); }

function bundle_report_getPrototypeOf(o) { bundle_report_getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return bundle_report_getPrototypeOf(o); }

function bundle_report_inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) bundle_report_setPrototypeOf(subClass, superClass); }

function bundle_report_setPrototypeOf(o, p) { bundle_report_setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return bundle_report_setPrototypeOf(o, p); }

function bundle_report_assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function bundle_report_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





var bundle_report_BundeReport =
/*#__PURE__*/
function (_Component) {
  bundle_report_inherits(BundeReport, _Component);

  function BundeReport() {
    var _getPrototypeOf2;

    var _this;

    bundle_report_classCallCheck(this, BundeReport);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = bundle_report_possibleConstructorReturn(this, (_getPrototypeOf2 = bundle_report_getPrototypeOf(BundeReport)).call.apply(_getPrototypeOf2, [this].concat(args)));

    bundle_report_defineProperty(bundle_report_assertThisInitialized(bundle_report_assertThisInitialized(_this)), "state", {
      value: 0,
      formData: {
        name: "",
        email: "",
        subscriptionType: "Bundle Subscription",
        message: "",
        productTitle: "",
        url: ""
      },
      status: "",
      popup: false
    });

    bundle_report_defineProperty(bundle_report_assertThisInitialized(bundle_report_assertThisInitialized(_this)), "handleChange", function () {
      _this.setState({
        value: 1
      });
    });

    bundle_report_defineProperty(bundle_report_assertThisInitialized(bundle_report_assertThisInitialized(_this)), "handleChangeInput", function (event) {
      var formData = bundle_report_objectSpread({}, _this.state.formData);

      formData[event.target.name] = event.target.value;

      _this.setState({
        formData: formData
      });
    });

    bundle_report_defineProperty(bundle_report_assertThisInitialized(bundle_report_assertThisInitialized(_this)), "handleSubmit", function (e) {
      e.preventDefault();

      var formData = bundle_report_objectSpread({}, _this.state.formData);

      formData.productTitle = document.title;
      formData.url = window.location.href;

      _this.setState({
        status: "Submitting Details",
        popup: true,
        formData: formData
      },
      /*#__PURE__*/
      bundle_report_asyncToGenerator(
      /*#__PURE__*/
      regenerator_default.a.mark(function _callee() {
        var formAPI, newData, response;
        return regenerator_default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                formAPI = "https://product-page-api.herokuapp.com/api/smtp/letUsHelp";
                newData = _this.state.formData;
                _context.next = 4;
                return external_axios_default.a.post(formAPI, newData);

              case 4:
                response = _context.sent;
                console.log(newData);
                console.log(response);

                _this.setState({
                  status: response.data.msg
                });

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      })));
    });

    return _this;
  }

  bundle_report_createClass(BundeReport, [{
    key: "render",
    value: function render() {
      return external_react_default.a.createElement("div", {
        className: "bundle-box"
      }, external_react_default.a.createElement("h3", null, "Could not find what you need?"), external_react_default.a.createElement("div", {
        className: "bundle-hide ".concat(this.state.value === 1 && "bundle-hidden")
      }, external_react_default.a.createElement("p", null, "Bundle Subscription or", external_react_default.a.createElement("br", null), " Customized reports"), external_react_default.a.createElement("button", {
        onClick: this.handleChange
      }, "Let Us Help You")), external_react_default.a.createElement("div", {
        className: "bundle-display ".concat(this.state.value === 0 && "bundle-hidden")
      }, external_react_default.a.createElement("p", null, "Let Us Help You"), external_react_default.a.createElement("form", {
        className: "bundle-form",
        onSubmit: this.handleSubmit
      }, external_react_default.a.createElement("input", {
        name: "name",
        required: true,
        placeholder: "name",
        value: this.state.formData.name,
        onChange: this.handleChangeInput
      }), external_react_default.a.createElement("input", {
        required: true,
        placeholder: "email",
        type: "email",
        name: "email",
        autoComplete: "email",
        value: this.state.formData.email,
        onChange: this.handleChangeInput
      }), external_react_default.a.createElement("select", {
        name: "subscriptionType",
        value: this.state.formData.subscriptionType,
        onChange: this.handleChangeInput
      }, external_react_default.a.createElement("option", null, "Bundle Subscription"), external_react_default.a.createElement("option", null, "Customized reports")), external_react_default.a.createElement("textarea", {
        placeholder: "Leave a Message",
        name: "message",
        value: this.state.formData.message,
        onChange: this.handleChangeInput
      }), external_react_default.a.createElement("button", {
        className: "bundle-submit"
      }, "Submit"), this.state.popup === true && external_react_default.a.createElement("p", null, this.state.status))));
    }
  }]);

  return BundeReport;
}(external_react_["Component"]);

/* harmony default export */ var components_bundle_report = (bundle_report_BundeReport);
// EXTERNAL MODULE: ./components/layout.jsx
var layout = __webpack_require__(12);

// EXTERNAL MODULE: external "styled-jsx/style"
var style_ = __webpack_require__(13);
var style_default = /*#__PURE__*/__webpack_require__.n(style_);

// EXTERNAL MODULE: ./scss/breadcrumb.scss
var breadcrumb = __webpack_require__(63);

// CONCATENATED MODULE: ./components/breadcrumb.jsx





var breadcrumb_Breadcrumb = function Breadcrumb(props) {
  return external_react_default.a.createElement("div", {
    className: "jsx-3227320165" + " " + "breadcrumb-box"
  }, external_react_default.a.createElement("p", {
    className: "jsx-3227320165"
  }, external_react_default.a.createElement("span", {
    className: "jsx-3227320165"
  }, external_react_default.a.createElement(link_default.a, {
    href: "/"
  }, external_react_default.a.createElement("a", {
    className: "jsx-3227320165"
  }, "Home")), " >", external_react_default.a.createElement("span", {
    className: "jsx-3227320165"
  }, external_react_default.a.createElement(link_default.a, {
    as: "/category/".concat(props.category.mainSelect),
    href: "/mainCategory?main=".concat(props.category.mainSelect)
  }, external_react_default.a.createElement("a", {
    className: "jsx-3227320165"
  }, props.category.mainSelect)), " >", external_react_default.a.createElement("span", {
    className: "jsx-3227320165"
  }, external_react_default.a.createElement(link_default.a, {
    as: "/category/".concat(props.category.mainSelect, "/").concat(props.category.productSelect),
    href: "/mainCategory?main=".concat(props.category.mainSelect, "&&product=").concat(props.category.productSelect)
  }, external_react_default.a.createElement("a", {
    className: "jsx-3227320165"
  }, props.category.productSelect)), " >", external_react_default.a.createElement("span", {
    className: "jsx-3227320165"
  }, external_react_default.a.createElement(link_default.a, {
    as: "/category/".concat(props.category.mainSelect, "/").concat(props.category.productSelect, "/").concat(props.category.subSelect),
    href: "/mainCategory?main=".concat(props.category.mainSelect, "&&product=").concat(props.category.productSelect, "&&sub=").concat(props.category.subSelect)
  }, external_react_default.a.createElement("a", {
    className: "jsx-3227320165"
  }, props.category.subSelect)), " >", external_react_default.a.createElement("span", {
    style: {
      color: "#FC5B01"
    },
    className: "jsx-3227320165"
  }, props.breadtitle === "" && props.Title, props.breadtitle)))))), external_react_default.a.createElement(style_default.a, {
    styleId: "3227320165",
    css: ["a.jsx-3227320165{-webkit-text-decoration:none;text-decoration:none;color:#a3a3a3;}"]
  }));
};

/* harmony default export */ var components_breadcrumb = (breadcrumb_Breadcrumb);
// EXTERNAL MODULE: ./scss/product-page.scss
var product_page = __webpack_require__(64);

// EXTERNAL MODULE: ./pages/cart.js
var cart = __webpack_require__(17);

// CONCATENATED MODULE: ./pages/product.js


function product_typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { product_typeof = function _typeof(obj) { return typeof obj; }; } else { product_typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return product_typeof(obj); }

function product_asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function product_asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { product_asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { product_asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function product_classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function product_defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function product_createClass(Constructor, protoProps, staticProps) { if (protoProps) product_defineProperties(Constructor.prototype, protoProps); if (staticProps) product_defineProperties(Constructor, staticProps); return Constructor; }

function product_possibleConstructorReturn(self, call) { if (call && (product_typeof(call) === "object" || typeof call === "function")) { return call; } return product_assertThisInitialized(self); }

function product_getPrototypeOf(o) { product_getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return product_getPrototypeOf(o); }

function product_inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) product_setPrototypeOf(subClass, superClass); }

function product_setPrototypeOf(o, p) { product_setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return product_setPrototypeOf(o, p); }

function product_assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function product_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
















var product_ProductPageTemplate =
/*#__PURE__*/
function (_Component) {
  product_inherits(ProductPageTemplate, _Component);

  function ProductPageTemplate() {
    var _getPrototypeOf2;

    var _this;

    product_classCallCheck(this, ProductPageTemplate);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = product_possibleConstructorReturn(this, (_getPrototypeOf2 = product_getPrototypeOf(ProductPageTemplate)).call.apply(_getPrototypeOf2, [this].concat(args)));

    product_defineProperty(product_assertThisInitialized(product_assertThisInitialized(_this)), "state", {
      keyURL: _this.props.data.slug,
      bgImage: "product-page-bg.jpg",
      PDate: "",
      // Category: {
      //   mainSelect: "",
      //   productSelect: "",
      //   subSelect: ""
      // },
      relatedReports: [],
      productTitle: _this.props.data.Product_Title,
      selectedPrice: "",
      slug: _this.props.data.slug,
      value: 0
    });

    product_defineProperty(product_assertThisInitialized(product_assertThisInitialized(_this)), "handlePriceRadioButton", function (value) {
      return function (e) {
        _this.setState({
          value: value,
          selectedPrice: e.target.value
        });
      };
    });

    product_defineProperty(product_assertThisInitialized(product_assertThisInitialized(_this)), "handlePriceSubmit", function (e) {
      e.preventDefault();
      console.log(_this.state);

      if (sessionStorage) {
        if (sessionStorage.getItem("cart")) {
          var prevCart = JSON.parse([sessionStorage.getItem("cart")]);
          var newItem = {
            productTitle: _this.state.productTitle,
            selectedPrice: _this.state.selectedPrice,
            slug: _this.props.data.slug,
            imagePath: _this.props.data.ImagePath
          };
          prevCart.push(newItem);
          sessionStorage.setItem("cart", JSON.stringify(prevCart));
        } else {
          sessionStorage.setItem("cart", JSON.stringify([{
            productTitle: _this.state.productTitle,
            selectedPrice: _this.state.selectedPrice,
            slug: _this.props.data.slug,
            imagePath: _this.props.data.ImagePath
          }]));
        } // sessionStorage.setItem("cart", [...sessionStorage.getItem("cart") || "", { productTitle: this.state.productTitle, selectedPrice: this.state.selectedPrice, slug: this.props.data.slug }]);


        router_default.a.push({
          pathname: "/cart",
          query: {
            productTitle: _this.state.productTitle,
            selectedPrice: _this.state.selectedPrice,
            slug: _this.props.data.slug,
            imagePath: _this.props.data.ImagePath
          }
        }, "/cart");
      }
    });

    return _this;
  }

  product_createClass(ProductPageTemplate, [{
    key: "componentDidMount",
    value: function () {
      var _componentDidMount = product_asyncToGenerator(
      /*#__PURE__*/
      regenerator_default.a.mark(function _callee() {
        var date, fullDate, RR;
        return regenerator_default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                date = this.props.data.created;
                fullDate = this.props.data.created.substring(0, date.indexOf("T"));
                _context.next = 4;
                return external_axios_default.a.get("https://product-page-api.herokuapp.com/api/products/getrr/".concat(this.props.data.Category.subSelect));

              case 4:
                RR = _context.sent;
                // const data = this.giveDate(fullDate);
                console.log(RR.data);
                this.setState({
                  PDate: fullDate,
                  relatedReports: RR.data
                });

              case 7:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function componentDidMount() {
        return _componentDidMount.apply(this, arguments);
      }

      return componentDidMount;
    }()
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(oldProps) {
      var _this2 = this;

      var newProps = this.props;

      if (oldProps !== newProps) {
        this.setState({
          keyURL: this.props.data.slug,
          PDate: "",
          Category: {
            mainSelect: "",
            productSelect: "",
            subSelect: ""
          },
          relatedReports: [],
          productTitle: this.props.data.Product_Title,
          selectedPrice: this.props.data.Price.Single,
          slug: this.props.data.slug,
          value: 1
        },
        /*#__PURE__*/
        product_asyncToGenerator(
        /*#__PURE__*/
        regenerator_default.a.mark(function _callee2() {
          var date, fullDate, RR;
          return regenerator_default.a.wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  date = _this2.props.data.created;
                  fullDate = _this2.props.data.created.substring(0, date.indexOf("T"));
                  _context2.next = 4;
                  return external_axios_default.a.get("https://product-page-api.herokuapp.com/api/products/getrr/".concat(_this2.props.data.Category.subSelect));

                case 4:
                  RR = _context2.sent;
                  // const data = this.giveDate(fullDate);
                  console.log(RR.data);

                  _this2.setState({
                    PDate: fullDate,
                    relatedReports: RR.data
                  });

                case 7:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2, this);
        })));
      }
    } // giveDate = date => {
    //   const data = date.indexOf("T");
    //   return data.substring(data);
    // };

  }, {
    key: "render",
    // handleChangeRequest = event => {
    //   const request = { ...this.state.request };
    //   request[event.target.name] = event.target.value;
    //   // request.product_title = document.title;
    //   // request.url = window.location.href;
    //   this.setState({ request });
    // };
    // handleRequestSubmit = async () => {
    //   const request = { ...this.state.request };
    //   request.product_title = " " + document.title;
    //   request.url = " " + window.location.href;
    //   this.setState({ request }, () => {
    //     const formAPI =
    //       "https://product-page-api.herokuapp.com/api/products/product/smtp";
    //     const newData = this.state.request;
    //     const { data: reply } = axios.post(formAPI, newData);
    //     console.log(newData);
    //     console.log(reply);
    //   });
    // };
    value: function render() {
      return external_react_default.a.createElement("div", null, external_react_default.a.createElement(head_default.a, null, external_react_default.a.createElement("title", null, this.props.data.Meta_title), external_react_default.a.createElement("meta", {
        name: "description",
        content: this.props.data.Meta_Desc
      }), external_react_default.a.createElement("meta", {
        name: "keywords",
        content: this.props.data.keywordArea.keyword
      })), external_react_default.a.createElement("div", {
        style: {
          backgroundImage: "url(/static/Images/product-page-bg.jpg)",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover"
        }
      }, external_react_default.a.createElement(layout["a" /* default */], {
        bgImage: this.state.bgImage,
        searchProps: this.props.searchData
      }, external_react_default.a.createElement("div", null, external_react_default.a.createElement(components_breadcrumb, {
        category: this.props.data.Category,
        breadtitle: this.props.data.BreadCrumb,
        Title: this.props.data.Product_Title
      }), external_react_default.a.createElement("div", {
        className: "clearfix",
        style: {
          paddingTop: "4rem"
        }
      }, external_react_default.a.createElement(product_main, {
        Title: this.props.data.Product_Title,
        ImagePath: this.props.data.ImagePath,
        PublishedDate: this.state.PDate,
        SKU: this.props.data.SKU
      })), external_react_default.a.createElement("div", {
        style: {
          backgroundColor: "#fff"
        }
      }, external_react_default.a.createElement("div", {
        className: "clearfix container"
      }, external_react_default.a.createElement("div", {
        className: "left-box"
      }, external_react_default.a.createElement(report_data_sec, {
        TabsData: this.props.data.Tabs
      })), external_react_default.a.createElement("div", {
        className: "right-box"
      }, external_react_default.a.createElement(components_buy_report, {
        Prices: this.props.data.Price,
        handleRadioButton: this.handlePriceRadioButton,
        handleSubmit: this.handlePriceSubmit,
        value: this.state.value,
        template: this.props.data.template
      }), external_react_default.a.createElement(components_bundle_report, null), external_react_default.a.createElement(related_reports_sec, {
        relatedReports: this.state.relatedReports
      }))))))));
    }
  }], [{
    key: "getInitialProps",
    value: function () {
      var _getInitialProps = product_asyncToGenerator(
      /*#__PURE__*/
      regenerator_default.a.mark(function _callee3(_ref2) {
        var req, res, keyurl, url, data, searchurl, searchData;
        return regenerator_default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                req = _ref2.req, res = _ref2.res, keyurl = _ref2.query.keyurl;
                _context3.next = 3;
                return external_isomorphic_unfetch_default()("https://product-page-api.herokuapp.com/api/products/product/".concat(keyurl));

              case 3:
                url = _context3.sent;
                _context3.next = 6;
                return url.json();

              case 6:
                data = _context3.sent;
                _context3.next = 9;
                return external_isomorphic_unfetch_default()("https://product-page-api.herokuapp.com/api/category/allData");

              case 9:
                searchurl = _context3.sent;
                _context3.next = 12;
                return searchurl.json();

              case 12:
                searchData = _context3.sent;

                if (!(data === null)) {
                  _context3.next = 20;
                  break;
                }

                res.statusCode = 404;
                res.set("application/html").send("<h1>Page not found</h1>\n      ");
                res.end("".concat(external_react_default.a.createElement("h1", null, "Page not found")));
                return _context3.abrupt("return");

              case 20:
                return _context3.abrupt("return", {
                  data: data,
                  searchData: searchData
                });

              case 21:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function getInitialProps(_x) {
        return _getInitialProps.apply(this, arguments);
      }

      return getInitialProps;
    }()
  }]);

  return ProductPageTemplate;
}(external_react_["Component"]);

/* harmony default export */ var product = __webpack_exports__["default"] = (Object(router_["withRouter"])(product_ProductPageTemplate)); // ProductPageTemplate.getInitialProps = async function(context) {
//   const { keyurl } = context.router.query.keyurl;
//   const urlkey = `https://product-page-api.herokuapp.com/api/products/product/${keyurl}`;
//   console.log({ keyurl });
//   const res = await fetch({ urlkey });
//   const data = await res.json();
//   console.log(`Show data fetched. Count: ${data}`);
//   return {
//     data
//   };
// };
// import { withRouter } from "next/router";
// const Page = withRouter(props => (
//   <div>
//     <h1>{props.router.query.title}</h1>
//     <p>This is the blog post content.</p>
//   </div>
// ));
// export default Page;

/***/ })
/******/ ]);