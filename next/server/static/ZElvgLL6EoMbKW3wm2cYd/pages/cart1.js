module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 26);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ 1:
/***/ (function(module, exports) {

module.exports = require("next/link");

/***/ }),

/***/ 13:
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ }),

/***/ 14:
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/regenerator");

/***/ }),

/***/ 26:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(27);


/***/ }),

/***/ 27:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(13);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(14);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_4__);



function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





var CartPage =
/*#__PURE__*/
function (_Component) {
  _inherits(CartPage, _Component);

  function CartPage() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, CartPage);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(CartPage)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      payInfo: {
        slug: _this.props.slug,
        firstName: "",
        lastName: "",
        companyName: "",
        country: "",
        streetAddress: "",
        townCity: "",
        state: "",
        postCode: "",
        phone: "",
        emailAddress: "",
        orderNote: "",
        itemName: _this.props.productTitle,
        itemPrice: _this.props.selectedPrice,
        sku: "DMM123",
        quantity: 1,
        totalPrice: _this.props.selectedPrice
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleChange", function (e) {
      var payInfo = _objectSpread({}, _this.state.payInfo);

      payInfo[e.target.name] = e.target.value;

      if (e.target.name === "quantity") {
        payInfo["totalPrice"] = payInfo["itemPrice"] * e.target.value;
      }

      _this.setState({
        payInfo: payInfo
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleSubmit", function (e) {
      e.preventDefault();
      console.log(_this.state);
      next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push({
        pathname: "/checkout",
        query: {
          slug: _this.state.payInfo.slug,
          firstName: _this.state.payInfo.firstName,
          lastName: _this.state.payInfo.lastName,
          companyName: _this.state.payInfo.companyName,
          country: _this.state.payInfo.country,
          streetAddress: _this.state.payInfo.streetAddress,
          townCity: _this.state.payInfo.townCity,
          state: _this.state.payInfo.state,
          postCode: _this.state.payInfo.postCode,
          phone: _this.state.payInfo.phone,
          emailAddress: _this.state.payInfo.emailAddress,
          orderNote: _this.state.payInfo.orderNote,
          itemName: _this.state.payInfo.itemName,
          itemPrice: _this.state.payInfo.itemPrice,
          sku: _this.state.payInfo.sku,
          quantity: _this.state.payInfo.quantity,
          totalPrice: _this.state.payInfo.totalPrice
        }
      }, "/checkout");
    });

    return _this;
  }

  _createClass(CartPage, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-1653222016" + " " + "cart"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
        className: "jsx-1653222016"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_4___default.a, {
        as: "/research-reports/".concat(this.state.payInfo.slug),
        href: "/product?keyurl=".concat(this.state.payInfo.slug)
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("a", {
        className: "jsx-1653222016"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("p", {
        className: "jsx-1653222016"
      }, this.props.productTitle, " - $", this.props.selectedPrice))), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-1653222016"
      }, "quantity", react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "number",
        name: "quantity",
        value: this.state.payInfo.quantity,
        onChange: this.handleChange,
        className: "jsx-1653222016"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("p", {
        className: "jsx-1653222016"
      }, "$", this.state.payInfo.totalPrice)), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("form", {
        onSubmit: this.handleSubmit,
        className: "jsx-1653222016"
      }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-1653222016"
      }, "First Name:", react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "text",
        name: "firstName",
        value: this.state.payInfo.firstName,
        onChange: this.handleChange,
        className: "jsx-1653222016"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-1653222016"
      }, "Last Name:", react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "text",
        name: "lastName",
        value: this.state.payInfo.lastName,
        onChange: this.handleChange,
        className: "jsx-1653222016"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-1653222016"
      }, "Company :", react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "text",
        name: "companyName",
        value: this.state.payInfo.companyName,
        onChange: this.handleChange,
        className: "jsx-1653222016"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-1653222016"
      }, "Country:", react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "text",
        name: "country",
        value: this.state.payInfo.country,
        onChange: this.handleChange,
        className: "jsx-1653222016"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-1653222016"
      }, "Street Address:", react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "text",
        name: "streetAddress",
        value: this.state.payInfo.streetAddress,
        onChange: this.handleChange,
        className: "jsx-1653222016"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-1653222016"
      }, "Town/City:", react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "text",
        name: "townCity",
        value: this.state.payInfo.townCity,
        onChange: this.handleChange,
        className: "jsx-1653222016"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-1653222016"
      }, "State:", react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "text",
        name: "state",
        value: this.state.payInfo.state,
        onChange: this.handleChange,
        className: "jsx-1653222016"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-1653222016"
      }, "Postal Code:", react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "number",
        name: "postCode",
        value: this.state.payInfo.postCode,
        onChange: this.handleChange,
        className: "jsx-1653222016"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-1653222016"
      }, "Phone:", react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "number",
        name: "phone",
        value: this.state.payInfo.phone,
        onChange: this.handleChange,
        className: "jsx-1653222016"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-1653222016"
      }, "Email:", react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "email",
        name: "emailAddress",
        value: this.state.payInfo.emailAddress,
        onChange: this.handleChange,
        className: "jsx-1653222016"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("label", {
        className: "jsx-1653222016"
      }, "Note:", react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
        type: "text",
        name: "orderNote",
        value: this.state.payInfo.orderNote,
        onChange: this.handleChange,
        className: "jsx-1653222016"
      })), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("button", {
        className: "jsx-1653222016"
      }, "Checkout")), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_1___default.a, {
        styleId: "1653222016",
        css: [".cart.jsx-1653222016{width:100%;max-width:135rem;}", "label.jsx-1653222016{display:block;float:left;width:50%;font-size:1.2rem;}", "input.jsx-1653222016{border:0.1rem solid #3b3b3b;padding:1rem;float:right;}", "button.jsx-1653222016{border:0.1rem solid red;padding:1rem;text-align:center;}"]
      }));
    }
  }], [{
    key: "getInitialProps",
    value: function () {
      var _getInitialProps = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(_ref) {
        var req, res, query;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                req = _ref.req, res = _ref.res, query = _ref.query;
                return _context.abrupt("return", query);

              case 2:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function getInitialProps(_x) {
        return _getInitialProps.apply(this, arguments);
      }

      return getInitialProps;
    }() // componentDidUpdate(oldProps) {
    //     const newProps = this.props
    //     if (oldProps !== newProps) {
    //         this.setState({
    //             slug: this.props.slug,
    //             // firstName: "",
    //             // lastName: "",
    //             // companyName: "",
    //             // country: "",
    //             // streetAddress: "",
    //             // townCity: "",
    //             // state: "",
    //             // postCode: "",
    //             // phone: "",
    //             // emailAddress: "",
    //             // orderNote: "",
    //             itemName: this.props.productTitle,
    //             itemPrice: this.props.selectedPrice,
    //             sku: "DMM123",
    //             quantity: 1,
    //             totalPrice: this.props.selectedPrice
    //         });
    //     }
    // }

  }]);

  return CartPage;
}(react__WEBPACK_IMPORTED_MODULE_2__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(next_router__WEBPACK_IMPORTED_MODULE_3__["withRouter"])(CartPage));

/***/ })

/******/ });