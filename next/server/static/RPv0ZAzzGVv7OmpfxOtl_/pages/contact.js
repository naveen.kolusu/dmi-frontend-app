module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 32);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("next/link");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/regenerator");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_search__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6);
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(7);
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(fuse_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(9);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _scss_navbar_scss__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(10);
/* harmony import */ var _scss_navbar_scss__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_scss_navbar_scss__WEBPACK_IMPORTED_MODULE_5__);


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






var ivData = [{
  name: "agriculture",
  ulData: [{
    name: "agrochemicals"
  }, {
    name: "seed"
  }, {
    name: "testing-technology"
  }]
}, {
  name: "animal-health",
  ulData: [{
    name: "feeds"
  }, {
    name: "veterinary"
  }]
}, {
  name: "automotive"
}, {
  name: "aviation"
}, {
  name: "energy-utilities",
  ulData: [{
    name: "power-generation"
  }]
}, {
  name: "food-beverages",
  ulData: [{
    name: "additives"
  }, {
    name: "beverages"
  }, {
    name: "processed-food"
  }, {
    name: "supplement"
  }]
}, {
  name: "healthcare-services",
  ulData: [{
    name: "healthcare-it"
  }]
}, {
  name: "information-communication"
}, {
  name: "medical-devices",
  ulData: [{
    name: "cardiovascular-devices"
  }, {
    name: "surgical-devices"
  }, {
    name: "wound-care"
  }]
}, {
  name: "metals-mining"
}, {
  name: "petrochemicals",
  ulData: [{
    name: "adhesives-sealants"
  }, {
    name: "advanced-materials"
  }, {
    name: "metals-ceramics"
  }, {
    name: "paints-coatings"
  }, {
    name: "seacial-fine-chemicals"
  }, {
    name: "sspeciality-chemicals"
  }, {
    name: "polymers"
  }, {
    name: "water-treatment"
  }]
}, {
  name: "pharmaceuticals",
  ulData: [{
    name: "oncology"
  }, {
    name: "ophthalmology"
  }]
}];
var paData = [{
  name: "pharmaceuticals-pa",
  ulData: [{
    name: "indication"
  }, {
    name: "moa"
  }, {
    name: "molecular-type"
  }]
}];

var NavBar =
/*#__PURE__*/
function (_React$Component) {
  _inherits(NavBar, _React$Component);

  function NavBar() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, NavBar);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(NavBar)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      // navBlock: true,
      searchBlock: false,
      searchInput: "",
      searchOutput: [],
      itemsToShow: {
        cats: {
          toggle: false,
          value: 0
        },
        subCats: {
          toggle: false,
          value: -1
        }
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleChange", function (e) {
      var data = _this.props.searchProps;
      console.log("newData", data);
      var options = {
        keys: ['Product_Title']
      };
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3___default.a(data, options); // const fuseData = [
      //   {
      //     Product_Title: "hello",
      //     slug: "hello"
      //   }
      // ]

      var fuseData = fuse.search(e.target.value);
      console.log(fuseData);

      _this.setState({
        searchInput: e.target.value,
        searchOutput: fuseData
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleClick", function (e) {
      if (e.target.name === "searchIcon") {
        _this.setState({
          // navBlock: false,
          searchBlock: true
        });
      } else {
        _this.setState({
          // navBlock: true,
          searchBlock: false
        });
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleToggle", function (cats, value) {
      var itemsToShow = _objectSpread({}, _this.state.itemsToShow);

      itemsToShow[cats].toggle = !itemsToShow[cats].toggle;
      itemsToShow[cats].value = value;

      _this.setState({
        itemsToShow: itemsToShow
      });

      console.log(itemsToShow);
    });

    return _this;
  }

  _createClass(NavBar, [{
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(newProps) {
      this.setState({
        searchBlock: false,
        searchInput: ""
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("nav", {
        className: "clearfix"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "image-box"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/static/Images/DMI-Logo-White.png",
        alt: "DMI-Logo"
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "nav-items-box"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
        className: "clearfix main-ul"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Home")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        as: "/category/industry-verticals",
        href: "/mainCategory?main=industry-verticals"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Industry Verticals"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: function onClick() {
          return _this2.handleToggle("cats", 1);
        },
        className: "arrow-down"
      }, "^"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
        className: " ".concat(this.state.itemsToShow.cats.value === 1 && this.state.itemsToShow.cats.toggle === true && "nav-cats")
      }, ivData.map(function (iv, ivindex) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
          key: ivindex,
          className: "nav-cats-item"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
          as: "/category/industry-verticals/".concat(iv.name),
          href: "/mainCategory?main=industry-verticals&&product?=".concat(iv.name)
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, iv.name))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
          onClick: function onClick() {
            return _this2.handleToggle("subCats", ivindex);
          },
          className: iv.ulData && "arrow"
        }, "^"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
          className: " ".concat(_this2.state.itemsToShow.subCats.value === ivindex && "nav-cats-inner")
        }, iv.ulData && iv.ulData.map(function (ul, ulindex) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
            key: ulindex,
            className: "nav-cats-item-inner"
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
            as: "/category/industry-verticals/".concat(iv.name, "/").concat(ul.name),
            href: "/mainCategory?main=industry-verticals&&product?=".concat(iv.name, "&&sub").concat(iv.name, "/").concat(ul.name)
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, ul.name))));
        })));
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        as: "/category/pipeline-analysis",
        href: "/mainCategory?main=pipeline-analysis"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Pipeline Analysis"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: function onClick() {
          return _this2.handleToggle("cats", 2);
        },
        className: "arrow-down"
      }, "^"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
        className: " ".concat(this.state.itemsToShow.cats.value === 2 && this.state.itemsToShow.cats.toggle === true && "nav-cats")
      }, paData.map(function (pa, paindex) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
          key: paindex,
          className: "nav-cats-item"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
          as: "/category/pipeline-analysis/".concat(pa.name),
          href: "/mainCategory?main=pipeline-analysis&&product?=".concat(pa.name)
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, pa.name))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
          onClick: function onClick() {
            return _this2.handleToggle("subCats", paindex);
          },
          className: pa.ulData && "arrow"
        }, "^"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
          className: " ".concat(_this2.state.itemsToShow.subCats.value === paindex && "nav-cats-inner")
        }, pa.ulData && pa.ulData.map(function (ul, ulindex) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
            key: ulindex,
            className: "nav-cats-item-inner"
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
            as: "/category/pipeline-analysis/".concat(pa.name, "/").concat(ul.name),
            href: "/mainCategory?main=pipeline-analysis&&product?=".concat(pa.name, "&&sub").concat(pa.name, "/").concat(ul.name)
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, ul.name))));
        })));
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Our Methodology")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        href: "/contact"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Contact Us")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        href: "/about"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "About Us")))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "nav-icons-box clearfix"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/static/icons/shopping-cart.svg",
        alt: "shopping-cart-icon"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/static/icons/search.svg",
        alt: "search-icon",
        onClick: this.handleClick,
        name: "searchIcon"
      }))), this.state.searchBlock === true && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "nav-searchbar"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_search__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], {
        handleChange: this.handleChange,
        searchInput: this.state.searchInput,
        searchOutput: this.state.searchOutput
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: this.handleClick,
        className: "nav-search-close"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/static/icons/multiply.svg"
      }))));
    }
  }]);

  return NavBar;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

;
/* harmony default export */ __webpack_exports__["a"] = (NavBar);
{
  /* <ul>
                   <li>
                     <Link as={`/category/industry-verticals/agriculture`} href={`/mainCategory?main=industry-verticals&&product=agriculture`}>
                       <a>
                         <p>Agriculture</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals/agriculture/agrochemicals`} href={`/mainCategory?main=industry-verticals&&product=agriculture&&sub=agrochemicals`}>
                           <a>
                             <p>Agrochemicals</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals/agriculture/seed`} href={`/mainCategory?main=industry-verticals&&product=agriculture&&sub=seed`}>
                           <a>
                             <p>Seed</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals/agriculture/seed`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Testing and Technology</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Animal Health</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Feeds</p>
                           </a>
                         </Link>
                       </li>
                       <li><Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                         <a>
                           <p>Veterinary</p>
                         </a>
                       </Link>
                       </li>
                     </ul>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Automotive</p>
                       </a>
                     </Link>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Aviation</p>
                       </a>
                     </Link>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Energy and Utilities</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Power Generation</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li><Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                     <a>
                       <p>Food and Beverages</p>
                     </a>
                   </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Additives</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Beverages</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Processed Food</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Supplements</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Healthcare Services</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Healthcare IT</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Information & Communication</p>
                       </a>
                     </Link>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Medical Devices</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Cardiovascular Devices</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Surgical Devices</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Woundcare Management</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li><Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                     <a>
                       <p>Metals and Mining</p>
                     </a>
                   </Link>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Petrochemicals</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Adhesives & Sealants</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Advanced Materials</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Metals ceramics & Industrial Materials</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Paints and Coatings</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Special and Fine Chemicals</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Speciality Chemicals</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Polymers</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Water Treatment</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li><Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                     <a>
                       <p>Pharmaceuticals</p>
                     </a>
                   </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Oncology</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Ophthalmology</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                 </ul> */
}

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _scss_footer_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(11);
/* harmony import */ var _scss_footer_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_scss_footer_scss__WEBPACK_IMPORTED_MODULE_4__);


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






var FooterSection =
/*#__PURE__*/
function (_Component) {
  _inherits(FooterSection, _Component);

  function FooterSection() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, FooterSection);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(FooterSection)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      formData: {
        preference: "Free Market Updates",
        email: "",
        interestIndustry: "",
        productTitle: "",
        url: ""
      },
      status: "",
      popup: false
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleChangeInput", function (event) {
      var formData = _objectSpread({}, _this.state.formData);

      formData[event.target.name] = event.target.value;

      _this.setState({
        formData: formData
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleSubmit", function (e) {
      e.preventDefault();

      var formData = _objectSpread({}, _this.state.formData);

      formData.productTitle = document.title;
      formData.url = window.location.href;

      _this.setState({
        status: "Submitting Details",
        popup: true,
        formData: formData
      },
      /*#__PURE__*/
      _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var formAPI, newData, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                formAPI = "https://product-page-api.herokuapp.com/api/smtp/footerForm";
                newData = _this.state.formData;
                _context.next = 4;
                return axios__WEBPACK_IMPORTED_MODULE_3___default.a.post(formAPI, newData);

              case 4:
                response = _context.sent;
                console.log(newData);
                console.log(response);

                _this.setState({
                  status: response.data.msg
                });

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      })));
    });

    return _this;
  }

  _createClass(FooterSection, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("footer", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "footer-box"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "container"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "footer"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "section1"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
        src: "/static/Images/DMI-Logo-White.png",
        alt: "Logo-White"
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "DataM Intelligence 4Market Research is a market intelligence platform which gives access to syndicated, customised reports to its clients at one place. As a firm with rich experience in research and consulting across multiple domains we are one stop solution that will cater to the needs of clients in key business areas.")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "section2"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Home"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Our Methodology"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Industry Verticals"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Careers"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Contact Us"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "About Us"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Terms & Conditions"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Privacy Policy"))))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "section3"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h3", null, " Contact "), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Corporate Address"), " ", react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), "1st floor, Phoenix Tech Tower, Plo no: 14/46, Habsidguda, IDA-Uppal, Hyderabad-500039, Telangana"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Email"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), " info@datamintelligence.com"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Phone "), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), "+1 877-441-4866")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "section4"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
        onSubmit: this.handleSubmit
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
        type: "email",
        name: "email",
        autoComplete: "email",
        placeholder: "Email",
        required: true,
        value: this.state.formData.email,
        onChange: this.handleChangeInput
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
        placeholder: "Industry of Interest",
        name: "interestIndustry",
        required: true,
        value: this.state.formData.interestIndustry,
        onChange: this.handleChangeInput
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("select", {
        required: true,
        name: "preference",
        value: this.state.formData.preference,
        onChange: this.handleChangeInput
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
        key: "1",
        value: "Free Market Updates"
      }, "Free Market Updates"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
        key: "2",
        value: "Newsletter"
      }, "Newsletter"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
        key: "3",
        value: "Reports"
      }, "Reports"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
        key: "4",
        value: "Offers"
      }, "Offers")), this.state.popup === true && react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, this.state.status), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", null, "Sign Up"))))))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "montaigne"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "container "
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Copyright \xA9 2019 DataM Intelligence. All Rights Reserved"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Designed and Developed by", react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        href: "www.montaigne.co"
      }, " Montaigne Labs")))));
    }
  }]);

  return FooterSection;
}(react__WEBPACK_IMPORTED_MODULE_1__["Component"]);

/* harmony default export */ __webpack_exports__["a"] = (FooterSection);

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }




var SearchPage =
/*#__PURE__*/
function (_Component) {
  _inherits(SearchPage, _Component);

  function SearchPage() {
    _classCallCheck(this, SearchPage);

    return _possibleConstructorReturn(this, _getPrototypeOf(SearchPage).apply(this, arguments));
  }

  _createClass(SearchPage, [{
    key: "render",
    // state = {
    //     searchInput: "",
    //     searchOutput: [],
    // }
    // static async getInitialProps() {
    //     const url = await fetch(
    //         `https://product-page-api.herokuapp.com/api/category/allData`
    //     );
    //     const data = await url.json();
    //     const newdata = {
    //         data: data
    //     }
    //     return newdata
    // }
    // handleChange = (e) => {
    //     const data = this.props.data;
    //     const options = {
    //         keys: ['Product_Title']
    //     }
    //     let fuse = new Fuse(data, options);
    //     const fuseData = fuse.search(e.target.value);
    //     console.log(fuseData);
    //     this.setState({
    //         searchInput: e.target.value,
    //         searchOutput: fuseData
    //     })
    // }
    value: function render() {
      var _this = this;

      var data = this.props.searchOutput;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "main-search-block"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        type: "search",
        value: this.props.searchInput,
        onChange: function onChange(e) {
          return _this.props.handleChange(e);
        },
        placeholder: "Start typing..."
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "search-result"
      }, data.slice(0, 9).map(function (s, index) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
          as: "/research-reports/".concat(s.slug),
          href: "/product?keyurl=".concat(s.slug)
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: s.ImagePath
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
          key: index
        }, s.Product_Title)));
      })));
    }
  }]);

  return SearchPage;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["a"] = (SearchPage);

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("fuse.js");

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("isomorphic-unfetch");

/***/ }),
/* 10 */
/***/ (function(module, exports) {



/***/ }),
/* 11 */
/***/ (function(module, exports) {



/***/ }),
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */,
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(33);


/***/ }),
/* 33 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(0);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_nav__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4);
/* harmony import */ var _components_footer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5);
/* harmony import */ var _scss_contact_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(34);
/* harmony import */ var _scss_contact_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_scss_contact_scss__WEBPACK_IMPORTED_MODULE_6__);


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








var ContactPage =
/*#__PURE__*/
function (_React$Component) {
  _inherits(ContactPage, _React$Component);

  function ContactPage() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, ContactPage);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(ContactPage)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      formData: {
        name: "",
        email: "",
        phone: "",
        organization: "",
        subject: "",
        message: "",
        productTitle: "",
        url: ""
      },
      status: "",
      popup: false
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleChangeInput", function (event) {
      var formData = _objectSpread({}, _this.state.formData);

      formData[event.target.name] = event.target.value;

      _this.setState({
        formData: formData
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleSubmit", function (e) {
      e.preventDefault();

      var formData = _objectSpread({}, _this.state.formData);

      formData.productTitle = document.title;
      formData.url = window.location.href;

      _this.setState({
        status: "Submitting Details",
        popup: true,
        formData: formData
      },
      /*#__PURE__*/
      _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var formAPI, newData, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                formAPI = "https://product-page-api.herokuapp.com/api/smtp/contactForm";
                newData = _this.state.formData;
                _context.next = 4;
                return axios__WEBPACK_IMPORTED_MODULE_3___default.a.post(formAPI, newData);

              case 4:
                response = _context.sent;
                console.log(newData);
                console.log(response);

                _this.setState({
                  status: response.data.msg
                });

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      })));
    });

    return _this;
  }

  _createClass(ContactPage, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("title", null, "Contact Us Page DMI")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "contact-box"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "contact-nav"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_nav__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"], {
        searchProps: this.props.data
      })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "contact-form-box"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h2", null, "LET US HELP YOU OUT"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
        className: "contact-form",
        onSubmit: this.handleSubmit
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "form-block"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "form-item"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", null, "Name"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
        name: "name",
        placeholder: "Enter Your Name",
        type: "text",
        required: true,
        value: this.state.formData.name,
        onChange: this.handleChangeInput
      })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "form-item"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", null, "Phone Number"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
        name: "phone",
        placeholder: "Enter Your Contact Number",
        type: "number",
        required: true,
        value: this.state.formData.phone,
        onChange: this.handleChangeInput
      }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "form-block"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "form-item"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", null, "Business Email"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
        name: "email",
        placeholder: "Enter Your Business Email",
        type: "email",
        required: true,
        value: this.state.formData.email,
        onChange: this.handleChangeInput
      })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "form-item"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", null, "Organization"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
        name: "organization",
        placeholder: "Enter Your Organization Name",
        type: "text",
        required: true,
        value: this.state.formData.organization,
        onChange: this.handleChangeInput
      }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "form-block"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "form-item-full"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", null, "What is your message about?"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("select", {
        name: "subject",
        value: this.state.formData.subject,
        onChange: this.handleChangeInput
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
        key: "3",
        value: "Ready Report Enquiry"
      }, "Ready Report Enquiry"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
        key: "3",
        value: "Tailor My Report"
      }, "Tailor My Report"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
        key: "3",
        value: "Expert Dicussion"
      }, "Expert Dicussion"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
        key: "3",
        value: "Request Free Demo"
      }, "Request Free Demo")))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "form-block"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "form-item-full"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", null, "Message"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("textarea", {
        placeholder: "Enter Your Message",
        name: "message",
        value: this.state.formData.message,
        onChange: this.handleChangeInput
      }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
        className: "form-item"
      }, "Submit"), this.state.popup === true && react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, this.state.status))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "contact-map-area"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        class: "mapouter"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        class: "gmap_canvas"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("iframe", {
        width: "1080",
        height: "631",
        id: "gmap_canvas",
        src: "https://maps.google.com/maps?q=datamintelligence&t=k&z=17&ie=UTF8&iwloc=&output=embed",
        frameborder: "0",
        scrolling: "no",
        marginheight: "0",
        marginwidth: "0"
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        href: "https://www.jetzt-drucken-lassen.de"
      })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        href: "https://www.embedgooglemap.net",
        rel: "nofollow",
        target: "_blank"
      }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "contact-details"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        style: {
          width: "33.33%"
        }
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "CONTACT"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        style: {
          display: "flex",
          alignItems: "center",
          justifyContent: "center"
        }
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
        src: "/static/Images/us-flag.png"
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "+1 877-441-4866"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        style: {
          width: "33.33%"
        }
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "wwww.datamintelligence.com"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "EMAIL"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", null, "Sales")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "info@datamintelligence.com"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", null, "PR & Media")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "media@datamintelligence.com")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        style: {
          width: "33.33%"
        }
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", null, "CORPORATE ADDRESS"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "1st floor, Phoenix Tech Tower,", react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), "Plot no: 14/46, Habsiguda, IDA-Uppal,", react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), "Hyderabad-500039, Telangana."))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_footer__WEBPACK_IMPORTED_MODULE_5__[/* default */ "a"], null)));
    }
  }], [{
    key: "getInitialProps",
    value: function () {
      var _getInitialProps = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var url, data, newdata;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return fetch("https://product-page-api.herokuapp.com/api/category/allData");

              case 2:
                url = _context2.sent;
                _context2.next = 5;
                return url.json();

              case 5:
                data = _context2.sent;
                newdata = {
                  data: data
                };
                return _context2.abrupt("return", newdata);

              case 8:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function getInitialProps() {
        return _getInitialProps.apply(this, arguments);
      }

      return getInitialProps;
    }()
  }]);

  return ContactPage;
}(react__WEBPACK_IMPORTED_MODULE_1___default.a.Component);

;
/* harmony default export */ __webpack_exports__["default"] = (ContactPage);

/***/ }),
/* 34 */
/***/ (function(module, exports) {



/***/ })
/******/ ]);