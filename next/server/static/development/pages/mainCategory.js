module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/footer.jsx":
/*!*******************************!*\
  !*** ./components/footer.jsx ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "@babel/runtime/regenerator");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/link */ "next/link");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _scss_footer_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../scss/footer.scss */ "./scss/footer.scss");
/* harmony import */ var _scss_footer_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_scss_footer_scss__WEBPACK_IMPORTED_MODULE_4__);


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






var FooterSection =
/*#__PURE__*/
function (_Component) {
  _inherits(FooterSection, _Component);

  function FooterSection() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, FooterSection);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(FooterSection)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      formData: {
        preference: "Free Market Updates",
        email: "",
        interestIndustry: "",
        productTitle: "",
        url: ""
      },
      status: "",
      popup: false
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleChangeInput", function (event) {
      var formData = _objectSpread({}, _this.state.formData);

      formData[event.target.name] = event.target.value;

      _this.setState({
        formData: formData
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleSubmit", function (e) {
      e.preventDefault();

      var formData = _objectSpread({}, _this.state.formData);

      formData.productTitle = document.title;
      formData.url = window.location.href;

      _this.setState({
        status: "Submitting Details",
        popup: true,
        formData: formData
      },
      /*#__PURE__*/
      _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var formAPI, newData, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                formAPI = "https://product-page-api.herokuapp.com/api/smtp/footerForm";
                newData = _this.state.formData;
                _context.next = 4;
                return axios__WEBPACK_IMPORTED_MODULE_3___default.a.post(formAPI, newData);

              case 4:
                response = _context.sent;
                console.log(newData);
                console.log(response);

                _this.setState({
                  status: response.data.msg
                });

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      })));
    });

    return _this;
  }

  _createClass(FooterSection, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("footer", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "footer-box"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "container"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "footer"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "section1"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
        src: "/static/Images/DMI-Logo-White.png",
        alt: "Logo-White"
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "DataM Intelligence 4Market Research is a market intelligence platform which gives access to syndicated, customised reports to its clients at one place. As a firm with rich experience in research and consulting across multiple domains we are one stop solution that will cater to the needs of clients in key business areas.")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "section2"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Home"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Our Methodology"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Industry Verticals"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Careers"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Contact Us"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "About Us"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Terms & Conditions"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, "Privacy Policy"))))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "section3"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h3", null, " Contact "), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Corporate Address"), " ", react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), "1st floor, Phoenix Tech Tower, Plo no: 14/46, Habsidguda, IDA-Uppal, Hyderabad-500039, Telangana"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Email"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), " info@datamintelligence.com"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "Phone "), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), "+1 877-441-4866")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "section4"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
        onSubmit: this.handleSubmit
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
        type: "email",
        name: "email",
        autoComplete: "email",
        placeholder: "Email",
        required: true,
        value: this.state.formData.email,
        onChange: this.handleChangeInput
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
        placeholder: "Industry of Interest",
        name: "interestIndustry",
        required: true,
        value: this.state.formData.interestIndustry,
        onChange: this.handleChangeInput
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("select", {
        required: true,
        name: "preference",
        value: this.state.formData.preference,
        onChange: this.handleChangeInput
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
        key: "1",
        value: "Free Market Updates"
      }, "Free Market Updates"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
        key: "2",
        value: "Newsletter"
      }, "Newsletter"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
        key: "3",
        value: "Reports"
      }, "Reports"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
        key: "4",
        value: "Offers"
      }, "Offers")), this.state.popup === true && react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, this.state.status), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", null, "Sign Up"))))))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "montaigne"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "container "
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Copyright \xA9 2019 DataM Intelligence. All Rights Reserved"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, "Designed and Developed by", react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        href: "www.montaigne.co"
      }, " Montaigne Labs")))));
    }
  }]);

  return FooterSection;
}(react__WEBPACK_IMPORTED_MODULE_1__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (FooterSection);

/***/ }),

/***/ "./components/nav.jsx":
/*!****************************!*\
  !*** ./components/nav.jsx ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "next/link");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_search__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/search */ "./components/search.jsx");
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! fuse.js */ "fuse.js");
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(fuse_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! isomorphic-unfetch */ "isomorphic-unfetch");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _scss_navbar_scss__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../scss/navbar.scss */ "./scss/navbar.scss");
/* harmony import */ var _scss_navbar_scss__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_scss_navbar_scss__WEBPACK_IMPORTED_MODULE_5__);


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






var ivData = [{
  name: "agriculture",
  ulData: [{
    name: "agrochemicals"
  }, {
    name: "seed"
  }, {
    name: "testing-technology"
  }]
}, {
  name: "animal-health",
  ulData: [{
    name: "feeds"
  }, {
    name: "veterinary"
  }]
}, {
  name: "automotive"
}, {
  name: "aviation"
}, {
  name: "energy-utilities",
  ulData: [{
    name: "power-generation"
  }]
}, {
  name: "food-beverages",
  ulData: [{
    name: "additives"
  }, {
    name: "beverages"
  }, {
    name: "processed-food"
  }, {
    name: "supplement"
  }]
}, {
  name: "healthcare-services",
  ulData: [{
    name: "healthcare-it"
  }]
}, {
  name: "information-communication"
}, {
  name: "medical-devices",
  ulData: [{
    name: "cardiovascular-devices"
  }, {
    name: "surgical-devices"
  }, {
    name: "wound-care"
  }]
}, {
  name: "metals-mining"
}, {
  name: "petrochemicals",
  ulData: [{
    name: "adhesives-sealants"
  }, {
    name: "advanced-materials"
  }, {
    name: "metals-ceramics"
  }, {
    name: "paints-coatings"
  }, {
    name: "seacial-fine-chemicals"
  }, {
    name: "sspeciality-chemicals"
  }, {
    name: "polymers"
  }, {
    name: "water-treatment"
  }]
}, {
  name: "pharmaceuticals",
  ulData: [{
    name: "oncology"
  }, {
    name: "ophthalmology"
  }]
}];
var paData = [{
  name: "pharmaceuticals-pa",
  ulData: [{
    name: "indication"
  }, {
    name: "moa"
  }, {
    name: "molecular-type"
  }]
}];

var NavBar =
/*#__PURE__*/
function (_React$Component) {
  _inherits(NavBar, _React$Component);

  function NavBar() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, NavBar);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(NavBar)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      // navBlock: true,
      searchBlock: false,
      searchInput: "",
      searchOutput: [],
      itemsToShow: {
        cats: {
          toggle: false,
          value: 0
        },
        subCats: {
          toggle: false,
          value: -1
        }
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleChange", function (e) {
      var data = _this.props.searchProps;
      console.log("newData", data);
      var options = {
        keys: ['Product_Title']
      };
      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_3___default.a(data, options); // const fuseData = [
      //   {
      //     Product_Title: "hello",
      //     slug: "hello"
      //   }
      // ]

      var fuseData = fuse.search(e.target.value);
      console.log(fuseData);

      _this.setState({
        searchInput: e.target.value,
        searchOutput: fuseData
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleClick", function (e) {
      if (e.target.name === "searchIcon") {
        _this.setState({
          // navBlock: false,
          searchBlock: true
        });
      } else {
        _this.setState({
          // navBlock: true,
          searchBlock: false
        });
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleToggle", function (cats, value) {
      var itemsToShow = _objectSpread({}, _this.state.itemsToShow);

      itemsToShow[cats].toggle = !itemsToShow[cats].toggle;
      itemsToShow[cats].value = value;

      _this.setState({
        itemsToShow: itemsToShow
      });

      console.log(itemsToShow);
    });

    return _this;
  }

  _createClass(NavBar, [{
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(newProps) {
      this.setState({
        searchBlock: false,
        searchInput: ""
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("nav", {
        className: "clearfix"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "image-box"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/static/Images/DMI-Logo-White.png",
        alt: "DMI-Logo"
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "nav-items-box"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
        className: "clearfix main-ul"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Home")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        as: "/category/industry-verticals",
        href: "/mainCategory?main=industry-verticals"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Industry Verticals"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: function onClick() {
          return _this2.handleToggle("cats", 1);
        },
        className: "arrow-down"
      }, "^"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
        className: " ".concat(this.state.itemsToShow.cats.value === 1 && this.state.itemsToShow.cats.toggle === true && "nav-cats")
      }, ivData.map(function (iv, ivindex) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
          key: ivindex,
          className: "nav-cats-item"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
          as: "/category/industry-verticals/".concat(iv.name),
          href: "/mainCategory?main=industry-verticals&&product?=".concat(iv.name)
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, iv.name))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
          onClick: function onClick() {
            return _this2.handleToggle("subCats", ivindex);
          },
          className: iv.ulData && "arrow"
        }, "^"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
          className: " ".concat(_this2.state.itemsToShow.subCats.value === ivindex && "nav-cats-inner")
        }, iv.ulData && iv.ulData.map(function (ul, ulindex) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
            key: ulindex,
            className: "nav-cats-item-inner"
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
            as: "/category/industry-verticals/".concat(iv.name, "/").concat(ul.name),
            href: "/mainCategory?main=industry-verticals&&product?=".concat(iv.name, "&&sub").concat(iv.name, "/").concat(ul.name)
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, ul.name))));
        })));
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        as: "/category/pipeline-analysis",
        href: "/mainCategory?main=pipeline-analysis"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Pipeline Analysis"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: function onClick() {
          return _this2.handleToggle("cats", 2);
        },
        className: "arrow-down"
      }, "^"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
        className: " ".concat(this.state.itemsToShow.cats.value === 2 && this.state.itemsToShow.cats.toggle === true && "nav-cats")
      }, paData.map(function (pa, paindex) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
          key: paindex,
          className: "nav-cats-item"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
          as: "/category/pipeline-analysis/".concat(pa.name),
          href: "/mainCategory?main=pipeline-analysis&&product?=".concat(pa.name)
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, pa.name))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
          onClick: function onClick() {
            return _this2.handleToggle("subCats", paindex);
          },
          className: pa.ulData && "arrow"
        }, "^"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
          className: " ".concat(_this2.state.itemsToShow.subCats.value === paindex && "nav-cats-inner")
        }, pa.ulData && pa.ulData.map(function (ul, ulindex) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
            key: ulindex,
            className: "nav-cats-item-inner"
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
            as: "/category/pipeline-analysis/".concat(pa.name, "/").concat(ul.name),
            href: "/mainCategory?main=pipeline-analysis&&product?=".concat(pa.name, "&&sub").concat(pa.name, "/").concat(ul.name)
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, ul.name))));
        })));
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Our Methodology")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        href: "/contact"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Contact Us")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
        href: "/about"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "About Us")))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "nav-icons-box clearfix"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/static/icons/shopping-cart.svg",
        alt: "shopping-cart-icon"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/static/icons/search.svg",
        alt: "search-icon",
        onClick: this.handleClick,
        name: "searchIcon"
      }))), this.state.searchBlock === true && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "nav-searchbar"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_search__WEBPACK_IMPORTED_MODULE_2__["default"], {
        handleChange: this.handleChange,
        searchInput: this.state.searchInput,
        searchOutput: this.state.searchOutput
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: this.handleClick,
        className: "nav-search-close"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "/static/icons/multiply.svg"
      }))));
    }
  }]);

  return NavBar;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

;
/* harmony default export */ __webpack_exports__["default"] = (NavBar);
{
  /* <ul>
                   <li>
                     <Link as={`/category/industry-verticals/agriculture`} href={`/mainCategory?main=industry-verticals&&product=agriculture`}>
                       <a>
                         <p>Agriculture</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals/agriculture/agrochemicals`} href={`/mainCategory?main=industry-verticals&&product=agriculture&&sub=agrochemicals`}>
                           <a>
                             <p>Agrochemicals</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals/agriculture/seed`} href={`/mainCategory?main=industry-verticals&&product=agriculture&&sub=seed`}>
                           <a>
                             <p>Seed</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals/agriculture/seed`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Testing and Technology</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Animal Health</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Feeds</p>
                           </a>
                         </Link>
                       </li>
                       <li><Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                         <a>
                           <p>Veterinary</p>
                         </a>
                       </Link>
                       </li>
                     </ul>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Automotive</p>
                       </a>
                     </Link>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Aviation</p>
                       </a>
                     </Link>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Energy and Utilities</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Power Generation</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li><Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                     <a>
                       <p>Food and Beverages</p>
                     </a>
                   </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Additives</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Beverages</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Processed Food</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Supplements</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Healthcare Services</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Healthcare IT</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Information & Communication</p>
                       </a>
                     </Link>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Medical Devices</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Cardiovascular Devices</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Surgical Devices</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Woundcare Management</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li><Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                     <a>
                       <p>Metals and Mining</p>
                     </a>
                   </Link>
                   </li>
                   <li>
                     <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                       <a>
                         <p>Petrochemicals</p>
                       </a>
                     </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Adhesives & Sealants</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Advanced Materials</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Metals ceramics & Industrial Materials</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Paints and Coatings</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Special and Fine Chemicals</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Speciality Chemicals</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Polymers</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Water Treatment</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                   <li><Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                     <a>
                       <p>Pharmaceuticals</p>
                     </a>
                   </Link>
                     <ul>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Oncology</p>
                           </a>
                         </Link>
                       </li>
                       <li>
                         <Link as={`/category/industry-verticals`} href={`/mainCategory?main=industry-verticals`}>
                           <a>
                             <p>Ophthalmology</p>
                           </a>
                         </Link>
                       </li>
                     </ul>
                   </li>
                 </ul> */
}

/***/ }),

/***/ "./components/search.jsx":
/*!*******************************!*\
  !*** ./components/search.jsx ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "next/link");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }




var SearchPage =
/*#__PURE__*/
function (_Component) {
  _inherits(SearchPage, _Component);

  function SearchPage() {
    _classCallCheck(this, SearchPage);

    return _possibleConstructorReturn(this, _getPrototypeOf(SearchPage).apply(this, arguments));
  }

  _createClass(SearchPage, [{
    key: "render",
    // state = {
    //     searchInput: "",
    //     searchOutput: [],
    // }
    // static async getInitialProps() {
    //     const url = await fetch(
    //         `https://product-page-api.herokuapp.com/api/category/allData`
    //     );
    //     const data = await url.json();
    //     const newdata = {
    //         data: data
    //     }
    //     return newdata
    // }
    // handleChange = (e) => {
    //     const data = this.props.data;
    //     const options = {
    //         keys: ['Product_Title']
    //     }
    //     let fuse = new Fuse(data, options);
    //     const fuseData = fuse.search(e.target.value);
    //     console.log(fuseData);
    //     this.setState({
    //         searchInput: e.target.value,
    //         searchOutput: fuseData
    //     })
    // }
    value: function render() {
      var _this = this;

      var data = this.props.searchOutput;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "main-search-block"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        type: "search",
        value: this.props.searchInput,
        onChange: function onChange(e) {
          return _this.props.handleChange(e);
        },
        placeholder: "Start typing..."
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "search-result"
      }, data.slice(0, 9).map(function (s, index) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
          as: "/research-reports/".concat(s.slug),
          href: "/product?keyurl=".concat(s.slug)
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: s.ImagePath
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
          key: index
        }, s.Product_Title)));
      })));
    }
  }]);

  return SearchPage;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (SearchPage);

/***/ }),

/***/ "./pages/mainCategory.js":
/*!*******************************!*\
  !*** ./pages/mainCategory.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "@babel/runtime/regenerator");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/link */ "next/link");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_nav__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/nav */ "./components/nav.jsx");
/* harmony import */ var _components_footer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/footer */ "./components/footer.jsx");
/* harmony import */ var _scss_category_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../scss/category.scss */ "./scss/category.scss");
/* harmony import */ var _scss_category_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_scss_category_scss__WEBPACK_IMPORTED_MODULE_6__);


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








var MainCategoryPage =
/*#__PURE__*/
function (_Component) {
  _inherits(MainCategoryPage, _Component);

  function MainCategoryPage() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, MainCategoryPage);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(MainCategoryPage)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {});

    return _this;
  }

  _createClass(MainCategoryPage, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "category-nav-box"
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_nav__WEBPACK_IMPORTED_MODULE_4__["default"], {
        searchProps: this.props.searchData
      })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "category-items-box"
      }, this.props.productData.map(function (cat, index) {
        return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "category-item"
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "category-content"
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
          src: cat.ImagePath
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, cat.Product_Title), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", null, "starting from ", react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", null, "$", cat.Price.Single))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "category-button"
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_3___default.a, {
          as: "/research-reports/".concat(cat.slug),
          href: "/product?keyurl=".concat(cat.slug)
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", null, "View report")))));
      }), this.props.productData.length === 0 && react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", null, "No Products Found")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_footer__WEBPACK_IMPORTED_MODULE_5__["default"], null));
    }
  }], [{
    key: "getInitialProps",
    value: function () {
      var _getInitialProps = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(_ref) {
        var req, res, query, url, main, sub, product, productData, searchurl, searchData;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                req = _ref.req, res = _ref.res, query = _ref.query;
                main = query.main;

                if (!(query.sub !== undefined)) {
                  _context.next = 9;
                  break;
                }

                sub = query.sub;
                _context.next = 6;
                return fetch("https://product-page-api.herokuapp.com/api/products/getSubCats/".concat(sub));

              case 6:
                url = _context.sent;
                _context.next = 19;
                break;

              case 9:
                if (!(query.product !== undefined)) {
                  _context.next = 16;
                  break;
                }

                product = query.product;
                _context.next = 13;
                return fetch("https://product-page-api.herokuapp.com/api/products/getProductCats/".concat(product));

              case 13:
                url = _context.sent;
                _context.next = 19;
                break;

              case 16:
                _context.next = 18;
                return fetch("https://product-page-api.herokuapp.com/api/products/getMainCats/".concat(main));

              case 18:
                url = _context.sent;

              case 19:
                _context.next = 21;
                return url.json();

              case 21:
                productData = _context.sent;
                _context.next = 24;
                return fetch("https://product-page-api.herokuapp.com/api/category/allData");

              case 24:
                searchurl = _context.sent;
                _context.next = 27;
                return searchurl.json();

              case 27:
                searchData = _context.sent;
                return _context.abrupt("return", {
                  query: query,
                  url: url,
                  productData: productData,
                  searchData: searchData
                });

              case 29:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function getInitialProps(_x) {
        return _getInitialProps.apply(this, arguments);
      }

      return getInitialProps;
    }()
  }]);

  return MainCategoryPage;
}(react__WEBPACK_IMPORTED_MODULE_1__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(next_router__WEBPACK_IMPORTED_MODULE_2__["withRouter"])(MainCategoryPage));

/***/ }),

/***/ "./scss/category.scss":
/*!****************************!*\
  !*** ./scss/category.scss ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./scss/footer.scss":
/*!**************************!*\
  !*** ./scss/footer.scss ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./scss/navbar.scss":
/*!**************************!*\
  !*** ./scss/navbar.scss ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ 4:
/*!*************************************!*\
  !*** multi ./pages/mainCategory.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./pages/mainCategory.js */"./pages/mainCategory.js");


/***/ }),

/***/ "@babel/runtime/regenerator":
/*!*********************************************!*\
  !*** external "@babel/runtime/regenerator" ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/regenerator");

/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),

/***/ "fuse.js":
/*!**************************!*\
  !*** external "fuse.js" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("fuse.js");

/***/ }),

/***/ "isomorphic-unfetch":
/*!*************************************!*\
  !*** external "isomorphic-unfetch" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("isomorphic-unfetch");

/***/ }),

/***/ "next/link":
/*!****************************!*\
  !*** external "next/link" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/link");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ })

/******/ });
//# sourceMappingURL=mainCategory.js.map